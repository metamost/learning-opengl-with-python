import ctypes, ctypes.util, logging, sys, textwrap
import numpy as np
from OpenGL import GL as gl
import glfw

log = logging.getLogger(__name__)


def load_shaders():
    shaders = {
        gl.GL_VERTEX_SHADER: textwrap.dedent('''\
            #version 330 core
            layout(location = 0) in vec3 vertexPosition_modelspace;
            void main(){
              gl_Position.xyz = vertexPosition_modelspace;
              gl_Position.w = 1.0;
            }
            '''),
        gl.GL_FRAGMENT_SHADER: textwrap.dedent('''\
            #version 330 core
            out vec3 color;
            void main(){
              color = vec3(1,0,0);
            }
            ''')
        }

    program_id = gl.glCreateProgram()

    shader_ids = []
    for shader_type, shader_src in shaders.items():
        shader_id = gl.glCreateShader(shader_type)
        gl.glShaderSource(shader_id, shader_src)
        gl.glCompileShader(shader_id)

        result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
        info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetShaderInfoLog(shader_id)
            log.error(logmsg)

        gl.glAttachShader(program_id, shader_id)
        shader_ids.append(shader_id)

    gl.glLinkProgram(program_id)

    result = gl.glGetProgramiv(program_id, gl.GL_LINK_STATUS)
    info_log_len = gl.glGetProgramiv(program_id, gl.GL_INFO_LOG_LENGTH)
    if info_log_len:
        logmsg = gl.glGetProgramInfoLog(program_id)
        log.error(logmsg)

    for shader_id in shader_ids:
        gl.glDetachShader(program_id, shader_id)
        gl.glDeleteShader(shader_id)

    return program_id


def create_window():
    if not glfw.init():
        log.error('Failed to initialize GLFW')
        sys.exit(1)

    glfw.window_hint(glfw.SAMPLES, 4)  # 4x antialiasing
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)  # To make MacOS happy
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    window = glfw.create_window(1024, 768, 'Tutorial 01', None, None)
    if not window:
        log.error('Failed to open GLFW window.')
        glfw.terminate()
        sys.exit(2)

    glfw.make_context_current(window)
    glfw.set_input_mode(window, glfw.STICKY_KEYS, True)
    gl.glClearColor(0, 0, 0.4, 0)
    return window


def initialize_glew():
    glew = ctypes.cdll.LoadLibrary(ctypes.util.find_library('GLEW'))
    glew.experimental = ctypes.c_bool.in_dll(glew, 'glewExperimental')
    glew.init = glew.glewInit
    glew.init.restype = int

    glew.experimental = True
    if glew.init():
        log.error('Failed to initialize GLEW')
        sys.exit(3)

def create_buffers():
    vertex_array_id = gl.glGenVertexArrays(1)
    gl.glBindVertexArray(vertex_array_id)

    vertex_data = [
        -1, -1, 0,
        1, -1, 0,
        0, 1, 0]

    vertex_buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)
    array_type = (gl.GLfloat * len(vertex_data))
    gl.glBufferData(gl.GL_ARRAY_BUFFER,
                    len(vertex_data) * ctypes.sizeof(ctypes.c_float),
                    array_type(*vertex_data),
                    gl.GL_STATIC_DRAW)

    return vertex_array_id, vertex_buffer


def main_loop(window, vertex_buffer, program_id):
    def maintain_window(window):
        result = glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS
        result = result and not glfw.window_should_close(window)
        return result

    while maintain_window(window):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        gl.glUseProgram(program_id)

        gl.glEnableVertexAttribArray(0)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)
        gl.glVertexAttribPointer(
           0,                  # attribute 0. No particular reason for 0,
                               # but must match the layout in the shader.
           3,                  # size
           gl.GL_FLOAT,        # type
           False,              # normalized?
           0,                  # stride
           None                # array buffer offset
        )

        # Draw the triangle !
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, 3)  # Starting from vertex 0
                                                # 3 vertices total -> 1 triangle
        gl.glDisableVertexAttribArray(0)

        glfw.swap_buffers(window)
        glfw.poll_events()


def cleanup(program_id, buffers, vertex_arrays):
    gl.glDeleteBuffers(len(buffers), buffers)
    gl.glDeleteVertexArrays(len(vertex_arrays), vertex_arrays)
    gl.glDeleteProgram(program_id)

    glfw.terminate()


def main():
    logging.basicConfig()
    window = create_window()
    initialize_glew()
    vertex_array_id, vertex_buffer = create_buffers()
    program_id = load_shaders()
    main_loop(window, vertex_buffer, program_id)
    cleanup(program_id, [vertex_buffer], [vertex_array_id])


if __name__ == '__main__':
    main()
