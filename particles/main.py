import contextlib, ctypes, logging, sys
import numpy as np
from numpy import random as rand
from OpenGL import GL as gl
import glfw

log = logging.getLogger(__name__)

@contextlib.contextmanager
def create_main_window():
    if not glfw.init():
        log.error('failed to initialize GLFW')
        sys.exit(1)
    try:
        log.debug('requiring modern OpenGL without any legacy features')
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

        log.debug('opening window')
        title = 'Point cloud'
        window = glfw.create_window(400*4, 400*4, title, None, None)
        if not window:
            log.error('failed to open GLFW window.')
            sys.exit(2)
        glfw.make_context_current(window)

        log.debug('listen for key presses')
        glfw.set_input_mode(window, glfw.STICKY_KEYS, True)

        log.debug('set background to dark blue')
        gl.glClearColor(0, 0, 0.4, 0)

        yield window

    finally:
        log.debug('terminating window context')
        glfw.terminate()

@contextlib.contextmanager
def create_vertex_array_object():
    log.debug('creating and binding the vertex array (VAO)')
    vertex_array_id = gl.glGenVertexArrays(1)
    try:
        gl.glBindVertexArray(vertex_array_id)
        yield
    finally:
        log.debug('cleaning up vertex array')
        gl.glDeleteVertexArrays(1, [vertex_array_id])

@contextlib.contextmanager
def create_vertex_buffer():
    with create_vertex_array_object():

        from sklearn.datasets import load_iris
        vertex_data = load_iris()['data'].astype(np.float32)
        stride = len(vertex_data.T)
        print(vertex_data.shape, stride, vertex_data.size, vertex_data.nbytes)
        for i in range(stride):
            dmin, dmax = vertex_data[:,i].min(), vertex_data[:,i].max()
            print(dmin, dmax)
            vertex_data[...,i] = (vertex_data[:,i] - dmin) / (dmax - dmin) - 0.5
        #vertex_data = rand.normal(0, 0.1, 4 * 10000).astype(np.float32)

        log.debug('creating and binding the vertex buffer (VBO)')
        vertex_buffer = gl.glGenBuffers(1)
        try:
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)

            gl.glBufferData(gl.GL_ARRAY_BUFFER,
                            vertex_data.nbytes,
                            vertex_data.ctypes.data_as(ctypes.POINTER(ctypes.c_float)),
                            gl.GL_STATIC_DRAW)

            log.debug('setting the vertex attributes')
            gl.glVertexAttribPointer(
               0,                  # attribute 0.
               1,                  # components per vertex attribute
               gl.GL_FLOAT,        # type
               False,              # to be normalized?
               stride * ctypes.sizeof(ctypes.c_float),
               ctypes.c_void_p(2 * ctypes.sizeof(ctypes.c_float)) # array buffer offset
            )
            gl.glEnableVertexAttribArray(0)  # use currently bound VAO

            gl.glVertexAttribPointer(
               1, 1, gl.GL_FLOAT, False, stride*ctypes.sizeof(ctypes.c_float),
               ctypes.c_void_p(0 * ctypes.sizeof(ctypes.c_float))
            )
            gl.glEnableVertexAttribArray(1)

            gl.glVertexAttribPointer(
               2, 1, gl.GL_FLOAT, False, stride*ctypes.sizeof(ctypes.c_float),
               ctypes.c_void_p(0 * ctypes.sizeof(ctypes.c_float))
            )
            gl.glEnableVertexAttribArray(2)

            yield
        finally:
            log.debug('cleaning up buffer')
            gl.glDisableVertexAttribArray(2)
            gl.glDisableVertexAttribArray(1)
            gl.glDisableVertexAttribArray(0)
            gl.glDeleteBuffers(1, [vertex_buffer])

@contextlib.contextmanager
def load_shaders():
    shaders = {
        gl.GL_VERTEX_SHADER: '''\
            #version 330 core
            layout(location = 0) in float x;
            layout(location = 1) in float y;
            layout(location = 2) in float z;
            out vec2 screenPos;
            out float radius;
            void main(){
              gl_Position.xyz = vec3(x, y, z);
              gl_PointSize = 30.0;

              vec2 halfsize = 0.5 * vec2(400*4, 400*4);
              screenPos = halfsize + (gl_Position.xy * halfsize);
              radius = 0.5 * gl_PointSize;
            }
            ''',
        gl.GL_FRAGMENT_SHADER: '''\
            #version 330 core
            out vec4 color;
            in vec2 screenPos;
            in float radius;
            void main(){
              float dist = distance(gl_FragCoord.xy, screenPos);
              if (dist > radius) discard;
              color = mix(vec4(1,0.5,0,1), vec4(1,0,0,0),
                          smoothstep(0, radius, dist));
            }
            '''
        }
    log.debug('enable point size function in shader')
    gl.glEnable(gl.GL_VERTEX_PROGRAM_POINT_SIZE)
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

    log.debug('creating the shader program')
    program_id = gl.glCreateProgram()
    try:
        shader_ids = []
        for shader_type, shader_src in shaders.items():
            shader_id = gl.glCreateShader(shader_type)
            gl.glShaderSource(shader_id, shader_src)

            log.debug(f'compiling the {shader_type} shader')
            gl.glCompileShader(shader_id)

            # check if compilation was successful
            result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
            info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
            if info_log_len:
                logmsg = gl.glGetShaderInfoLog(shader_id)
                log.error(logmsg)
                sys.exit(10)

            gl.glAttachShader(program_id, shader_id)
            shader_ids.append(shader_id)

        log.debug('linking shader program')
        gl.glLinkProgram(program_id)

        # check if linking was successful
        result = gl.glGetProgramiv(program_id, gl.GL_LINK_STATUS)
        info_log_len = gl.glGetProgramiv(program_id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetProgramInfoLog(program_id)
            log.error(logmsg)
            sys.exit(11)

        log.debug('installing shader program into rendering state')
        gl.glUseProgram(program_id)
        yield
    finally:
        log.debug('cleaning up shader program')
        for shader_id in shader_ids:
            gl.glDetachShader(program_id, shader_id)
            gl.glDeleteShader(shader_id)
        gl.glUseProgram(0)
        gl.glDeleteProgram(program_id)

def main_loop(window):
    while (
        glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS and
        not glfw.window_should_close(window)
    ):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        # Draw the points
        gl.glDrawArrays(gl.GL_POINTS, 0, 150)
        glfw.swap_buffers(window)
        glfw.poll_events()

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    with create_main_window() as window:
        with create_vertex_buffer():
            with load_shaders():
                main_loop(window)
