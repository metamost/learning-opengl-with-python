import ctypes
import ctypes.util
import copy
import logging
import sys

import numpy as np
import quaternion
from OpenGL import GL as gl

from window import WindowGL as Window
from transform import MVPTransformGL as MVPTransform
from scene import InteractiveScene
from shader import Axes, ColorfulShapes, Target


log = logging.getLogger(__name__)


# this probably belongs next to the shaders or the gl window
def initialize_glew():
    glew = ctypes.cdll.LoadLibrary(ctypes.util.find_library('GLEW'))
    glew.experimental = ctypes.c_bool.in_dll(glew, 'glewExperimental')
    glew.glewInit.restype = int
    glew.init = glew.glewInit
    glew.glewGetErrorString.argtypes = (ctypes.c_int,)
    glew.glewGetErrorString.restype = ctypes.c_char_p
    glew.getErrorString = lambda err: glew.glewGetErrorString(err).decode()

    glew.experimental = True
    if err := glew.init():
        log.error(f'Failed to initialize GLEW (error code: {err})')
        log.error(glew.getErrorString(err))
        sys.exit(3)


def main():
    mvp = MVPTransform()
    mvp.view.eye = np.ones(3) * 30
    mvp.view.look_at(np.ones(3) * 10, np.array((0,1,0)))
    mvp.projection.near = 0.1
    mvp.projection.far = 100

    window = Window('Colorful Shapes')
    scene = InteractiveScene(window, mvp)
    axes = Axes(scene.mvp)
    target = Target(scene.mvp)
    colorful_shapes = ColorfulShapes(scene.mvp)
    with (
        window,
        scene,
        axes,
        target,
        colorful_shapes,
    ):
        gl.glEnable(gl.GL_MULTISAMPLE)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glDepthFunc(gl.GL_LESS)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        #initialize_glew()
        for _ in window.draw_loop():
            gl.glDepthMask(gl.GL_TRUE)  # apply depth mask for opaque objects
            colorful_shapes.draw()
            axes.draw()
            gl.glDepthMask(gl.GL_FALSE)  # do not write depth for translucent objects
                                         # these must be in order if blending is order-dependent
            gl.glEnable(gl.GL_BLEND)
            target.draw(scene.mvp.view.target)
            gl.glDisable(gl.GL_BLEND)
            gl.glDepthMask(gl.GL_TRUE)



if __name__ == '__main__':
    np.set_printoptions(precision=3)
    logging.basicConfig(level=logging.DEBUG)
    main()
