import numpy as np


class Coordinates(np.ndarray):
    def __new__(cls, input_array, **kwargs):
        return np.asarray(input_array, **kwargs).view(cls)

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, value):
        self[0] = value

    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, value):
        self[1] = value

    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, value):
        self[2] = value

    @property
    def w(self):
        return self[3]

    @w.setter
    def w(self, value):
        self[2] = value

    @property
    def xy(self):
        return Coordinates(self[:2])

    @property
    def xyz(self):
        return Coordinates(self[:3])

    def __eq__(self, other):
        return np.array_equal(self, other)

    def __ne__(self, other):
        return not (self == other)


class Size(np.ndarray):
    def __new__(cls, input_array, **kwargs):
        return np.asarray(input_array, **kwargs).view(cls)

    @property
    def width(self):
        return self[0]

    @width.setter
    def width(self, value):
        self[0] = value

    @property
    def height(self):
        return self[1]

    @height.setter
    def height(self, value):
        self[1] = value

    def __eq__(self, other):
        return np.array_equal(self, other)

    def __ne__(self, other):
        return not (self == other)


class Key:
    Up = 0
    Down = 1
    Left = 2
    Right = 3


class Button:
    MouseLeft = 0
    MouseRight = 1
    MouseMiddle = 2


class Action:
    Press = 0
    Repeat = 1
    Release = 2


class Mod:
    Shift = 1 << 0
    Control = 1 << 1
    Alt = 1 << 2
    Super = 1 << 3
    CapsLock = 1 << 4
    NumLock = 1 << 5
