import logging
import sys

from OpenGL import GL as gl
import glfw

from util import Coordinates, Size, Action, Key, Button, Mod


log = logging.getLogger(__name__)


class WindowGL:
    def __init__(self, title=None, size=(1024, 768)):
        width, height = Size(size, dtype=int)

        if not glfw.init():
            log.error('Failed to initialize GLFW')
            sys.exit(1)

        glfw.window_hint(glfw.SAMPLES, 4)  # 4x antialiasing
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)  # To make MacOS happy
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

        self.window = glfw.create_window(width, height, title, None, None)
        if not self.window:
            log.error('Failed to open GLFW window.')
            glfw.terminate()
            sys.exit(2)

    def __enter__(self):
        glfw.make_context_current(self.window)
        glfw.set_input_mode(self.window, glfw.STICKY_KEYS, False)
        gl.glClearColor(0, 0, 0.4, 0)

    def __exit__(self, *args):
        glfw.make_context_current(None)

    def __del__(self):
        glfw.terminate()

    def maintain(self):
        result = glfw.get_key(self.window, glfw.KEY_ESCAPE) != glfw.PRESS
        return result and not glfw.window_should_close(self.window)

    def clear(self):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

    def swap_buffers(self):
        glfw.swap_buffers(self.window)

    def poll_events(self):
        glfw.poll_events()

    def draw_loop(self):
        while self.maintain():
            gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
            yield
            glfw.swap_buffers(self.window)
            glfw.poll_events()

    def window_size(self):
        return Size(glfw.get_window_size(self.window), dtype=int)

    def framebuffer_size(self):
        return Size(glfw.get_framebuffer_size(self.window), dtype=int)

    def cursor_window_pos(self):
        '''
            cursor position in window/screen display coordinates
            display pixels (x, y) with origin at left top
                units: pixels
                origin: left top
                x: left to right
                y: top to bottom
        '''
        pos = Coordinates(glfw.get_cursor_pos(self.window), dtype=int)
        log.debug(f'  CURSOR WIN POS: {pos}')
        return pos

    def normalized_depth_at_pos(self, x, y):
        '''
            depth at position (x, y) of screen normalized to range [-1, 1]
        '''
        depth = gl.glReadPixels(x, y, 1, 1, gl.GL_DEPTH_COMPONENT, gl.GL_FLOAT)
        return 2 * depth[0][0] - 1

    @staticmethod
    def Key(glfw_key):
        return {
            glfw.KEY_UP: Key.Up,
            glfw.KEY_DOWN: Key.Down,
            glfw.KEY_LEFT: Key.Left,
            glfw.KEY_RIGHT: Key.Right,
        }.get(glfw_key, None)

    @staticmethod
    def Button(glfw_button):
        return {
            glfw.MOUSE_BUTTON_LEFT: Button.MouseLeft,
            glfw.MOUSE_BUTTON_RIGHT: Button.MouseRight,
            glfw.MOUSE_BUTTON_MIDDLE: Button.MouseMiddle,
        }.get(glfw_button, None)

    @staticmethod
    def Action(glfw_action):
        return {
            glfw.PRESS: Action.Press,
            glfw.REPEAT: Action.Repeat,
            glfw.RELEASE: Action.Release,
        }.get(glfw_action, None)

    @staticmethod
    def Mods(glfw_mods):
        result = 0
        result |= Mod.Shift if (glfw_mods & glfw.MOD_SHIFT) else 0
        result |= Mod.Control if (glfw_mods & glfw.MOD_CONTROL) else 0
        result |= Mod.Alt if (glfw_mods & glfw.MOD_ALT) else 0
        result |= Mod.Super if (glfw_mods & glfw.MOD_SUPER) else 0
        result |= Mod.CapsLock if (glfw_mods & glfw.MOD_CAPS_LOCK) else 0
        result |= Mod.NumLock if (glfw_mods & glfw.MOD_NUM_LOCK) else 0
        return result

    def modifiers(self):
        lctrl = glfw.get_key(self.window, glfw.KEY_LEFT_CONTROL) == glfw.PRESS
        rctrl = glfw.get_key(self.window, glfw.KEY_RIGHT_CONTROL) == glfw.PRESS
        lshft = glfw.get_key(self.window, glfw.KEY_LEFT_SHIFT) == glfw.PRESS
        rshft = glfw.get_key(self.window, glfw.KEY_RIGHT_SHIFT) == glfw.PRESS
        lalt = glfw.get_key(self.window, glfw.KEY_LEFT_ALT) == glfw.PRESS
        ralt = glfw.get_key(self.window, glfw.KEY_RIGHT_ALT) == glfw.PRESS
        result = 0
        result |= Mod.Shift if (lshft or rshft) else 0
        result |= Mod.Control if (lctrl or rctrl) else 0
        result |= Mod.Alt if (lalt or ralt) else 0
        return result

    def set_cursor_pos_callback(self, fn=None):
        '''
            GLFW callback signature:
                void function_name(GLFWwindow* window, double xpos, double ypos);
            Py callback signature:
                fn(pos, mods)
        '''
        if fn is None:
            glfw.set_cursor_pos_callback(self.window, lambda *a, **kw: None)
        else:
            def callback(window, x, y, fn=fn):
                pos = Coordinates((x, y))
                mods = self.modifiers()
                fn(pos, mods)
            glfw.set_cursor_pos_callback(self.window, callback)

    def set_key_callback(self, fn=None):
        if fn is None:
            glfw.set_key_callback(self.window, lambda *a, **kw: None)
        else:
            def callback(window, key, scancode, action, mods, fn=fn):
                key = WindowGL.Key(key)
                action = WindowGL.Action(action)
                mods = WindowGL.Mods(mods)
                fn(key, action, mods)
            glfw.set_key_callback(self.window, callback)


    def set_scroll_callback(self, fn=None):
        if fn is None:
            glfw.set_scroll_callback(self.window, lambda *a, **kw: None)
        else:
            def callback(window, xoffset, yoffset, fn=fn):
                offset = Coordinates((xoffset, yoffset))
                mods = self.modifiers()
                fn(offset, mods)
            glfw.set_scroll_callback(self.window, callback)

    def set_mouse_button_callback(self, fn=None):
        if fn is None:
            glfw.set_mouse_button_callback(self.window, lambda *a, **kw: None)
        else:
            def callback(window, button, action, mods, fn=fn):
                button = WindowGL.Button(button)
                action = WindowGL.Action(action)
                mods = WindowGL.Mods(mods)
                fn(button, action, mods)
            glfw.set_mouse_button_callback(self.window, callback)
