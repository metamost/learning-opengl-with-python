import copy
import logging

import numpy as np
import quaternion

from transform import rotation_between_vectors
from util import Coordinates, Action, Key, Button, Mod


log = logging.getLogger(__name__)


class config:
    class view:
        class controls:
            class arcball:
                xscale = 2.1 * np.pi
                yscale = 2.1 * np.pi
                axis = np.array((0, 1, 0))
            class trackball:
                xscale = 1.1 * np.pi
                yscale = 1.1 * np.pi
            class rotate:
                scale = 1.0
            class fov:
                min = 25 * np.pi / 180
                max = 85 * np.pi / 180
                step = 1 * np.pi / 180
            class translate:
                forward_step = 1
                right_step = 1
                xscale = 1.0
                yscale = 1.0


class Scene:
    def __init__(self, window, mvp):
        self.window = window
        self.mvp = mvp

    def cursor_normalized_window_pos(self, window_pos=None):
        '''
            cursor position (center of pixel) (x, y) where
            x, y are normalized to the range [-1, 1]
                origin: center
                x: left to right
                y: top to bottom
        '''
        if window_pos is None:
            window_pos = self.window.cursor_window_pos()
        else:
            window_pos = Coordinates(window_pos)
        win_size = self.window.window_size()
        pos = 2 * (window_pos + Coordinates((0.5, 0.5))) / win_size - 1
        pos.y *= -1
        log.debug(f'  CURSOR NORM WIN POS: {pos}')
        return pos

    def cursor_framebuffer_pos(self, window_pos=None):
        '''
            cursor position in framebuffer coordinates
            framebuffer pixels (x, y) with origin at left top
                units: pixels
                origin: left top
                x: left to right
                y: top to bottom
        '''
        if window_pos is None:
            window_pos = self.window.cursor_window_pos()
        else:
            window_pos = Coordinates(window_pos)
        scaling = self.window.framebuffer_size() / self.window.window_size()
        pos = window_pos * scaling
        log.debug(f'  CURSOR FBUF POS: {pos.astype(int)}')
        return pos

    def cursor_normalized_pos(self, framebuffer_pos=None):
        '''
            normalized device (screen) coordinates (x, y, z, w=1)
            where x, y, z are in the range [-1, 1]
                origin: center
                x: left to right
                y: top to bottom
                z: out of the screen to the viewer
        '''
        if framebuffer_pos is None:
            framebuffer_pos = self.cursor_framebuffer_pos()
        else:
            framebuffer_pos = Coordinates(framebuffer_pos)

        # cursor position of pixel center in framebuffer coordinates
        # units: (pixel width, pixel height)
        # origin: left top
        pos = framebuffer_pos + Coordinates((0.5, 0.5))

        # get depth at cursor position
        # range: [0, 1]
        fb_size = self.window.framebuffer_size()
        y = fb_size.height - pos.y - 1
        z = self.window.normalized_depth_at_pos(pos.x, y)

        # cursor position in normalized device (screen) coordinates
        # where x, y, z are in the range [-1, 1]
        pos = 2 * pos / fb_size - 1
        pos = Coordinates((pos.x, - pos.y, z, 1))
        log.debug(f'  CURSOR NORM POS: {pos}')
        return pos

    def cursor_camera_pos(self, normalized_pos=None, inv_proj=None):
        '''
            position in camera (view) coordinates (x, y, z, w=1)
            cursor position in world space where the origin is the camera
            position
                z-axis: forward
                y-axis: up              ??? right-handed ???
                x-axis: right           ???              ???
        '''
        if normalized_pos is None:
            normalized_pos = self.cursor_normalized_pos()
        else:
            normalized_pos = Coordinates(normalized_pos)
        if inv_proj is None:
            inv_proj = self.mvp.projection.inv_matrix
        pos = normalized_pos @ inv_proj
        pos /= pos.w
        log.debug(f'  CURSOR CAMERA POS: {pos}')
        return pos

    def cursor_world_pos(self, camera_pos=None, inv_view=None):
        '''
            cursor position in world coordinates (x, y, z, w=1)
        '''
        if camera_pos is None:
            camera_pos = self.cursor_camera_pos()
        else:
            camera_pos = Coordinates(camera_pos)
        if inv_view is None:
            inv_view = self.mvp.view.inv_matrix
        pos = camera_pos @ inv_view
        pos /= pos.w
        log.debug(f'  CURSOR WORLD POS: {pos}')
        return pos

    def eye_to_cursor(self, world_pos=None):
        '''
            eye to cursor vector in world space
        '''
        if world_pos is None:
            world_pos = self.cursor_world_pos()
        else:
            world_pos = Coordinates(world_pos)
        return world_pos.xyz - self.mvp.view.eye

    ################ ARCBALL ###################################################
    def start_arcball(self):
        if not hasattr(self, 'stop_view_change'):
            self.initial_cursor_pos = self.cursor_normalized_window_pos()
            self.initial_view = copy.copy(self.mvp.view)
            self.on_cursor_pos = self.arcball
            self.window.set_cursor_pos_callback(self.on_cursor_pos)
            self.stop_view_change = self.stop_arcball

    def stop_arcball(self):
        del self.stop_view_change
        self.window.set_cursor_pos_callback()
        del self.on_cursor_pos
        del self.initial_view
        del self.initial_cursor_pos

    def arcball(self, pos, mods):
        xscale = config.view.controls.arcball.xscale
        yscale = config.view.controls.arcball.yscale

        p0 = self.initial_cursor_pos
        v0 = self.initial_view
        p1 = self.cursor_normalized_window_pos(pos)

        if mods & Mod.Control:
            log.debug(f'ARCBALL {p0} -> {p1}')
            upvec = xscale * (p0.x - p1.x) * v0.up
            rightvec = yscale * (p1.y - p0.y) * v0.right
            rotation = quaternion.from_rotation_vector(upvec + rightvec)
        elif mods & Mod.Alt:
            log.debug(f'CAMERA-UP-AXIS ARCBALL {p0} -> {p1}')
            upvec = xscale * (p0.x - p1.x) * v0.up
            uprot = quaternion.from_rotation_vector(upvec)
            newright = quaternion.as_rotation_matrix(uprot) @ v0.right
            newrightvec = yscale * (p1.y - p0.y) * newright
            rightrot = quaternion.from_rotation_vector(newrightvec)
            rotation = rightrot * uprot
        else:
            log.debug(f'WORLD-AXIS ARCBALL {p0} -> {p1}')
            # maybe better to use the closest axis to the "up" direction
            axis = config.view.controls.arcball.axis
            axangle = xscale * (p0.x - p1.x) * axis
            axrot = quaternion.from_rotation_vector(axangle)
            rangle = yscale * (p1.y - p0.y) * np.cross(v0.forward, axis)
            rrot = quaternion.from_rotation_vector(rangle)
            rotation = axrot * rrot

        self.mvp.view = copy.copy(v0)
        self.mvp.view.rotate_about(rotation, v0.target)

    ################ TRACKBALL #################################################
    def start_trackball(self):
        if not hasattr(self, 'stop_view_change'):
            self.initial_cursor_pos = self.cursor_normalized_window_pos()
            self.window.set_cursor_pos_callback(self.trackball)
            self.stop_view_change = self.stop_trackball

    def stop_trackball(self):
        del self.stop_view_change
        self.window.set_cursor_pos_callback()
        del self.initial_cursor_pos

    def trackball(self, pos, mods):
        xscale = config.view.controls.trackball.xscale
        yscale = config.view.controls.trackball.yscale

        p0 = self.initial_cursor_pos
        p1 = self.cursor_normalized_window_pos(pos)
        log.debug(f'TRACKBALL {p0} -> {p1}')

        upvec = xscale * (p0.x - p1.x) * self.mvp.view.up
        rightvec = yscale * (p1.y - p0.y) * self.mvp.view.right
        rotation = quaternion.from_rotation_vector(upvec + rightvec)

        self.mvp.view.rotate_about(rotation, self.mvp.view.target)
        self.initial_cursor_pos = Coordinates(p1)

    ############## ROTATE ######################################################
    def start_rotate(self):
        if not hasattr(self, 'stop_view_change'):
            norm_pos = self.cursor_normalized_pos()
            camera_pos = self.cursor_camera_pos(norm_pos)
            pos = self.cursor_world_pos(camera_pos).xyz
            self.initial_norm_z = norm_pos.z
            self.initial_eye_to_cursor = pos - self.mvp.view.eye
            self.initial_view = copy.copy(self.mvp.view)
            self.window.set_cursor_pos_callback(self.rotate)
            self.stop_view_change = self.stop_rotate

    def stop_rotate(self):
        del self.stop_view_change
        self.window.set_cursor_pos_callback()
        del self.initial_view
        del self.initial_eye_to_cursor
        del self.initial_norm_z

    def rotate(self, pos, mods):
        scale = config.view.controls.rotate.scale

        v0 = self.initial_eye_to_cursor
        x1, y1 = self.cursor_normalized_window_pos(pos)
        norm_pos = np.array((x1, y1, self.initial_norm_z, 1))
        camera_pos = self.cursor_camera_pos(norm_pos)
        ini_pos = self.cursor_world_pos(camera_pos, self.initial_view.inv_matrix).xyz
        v1 = ini_pos - self.initial_view.eye

        log.debug(f'ROTATE {v0} -> {v1}')
        rotation = rotation_between_vectors(v1, v0)
        rotation.w /= scale

        self.mvp.view = copy.copy(self.initial_view)
        self.mvp.view.rotate(rotation)

    ############## TRANSLATE ###################################################
    def start_translate(self):
        if not hasattr(self, 'stop_view_change'):
            norm_pos = self.cursor_normalized_pos()
            self.initial_norm_z = norm_pos.z
            self.initial_pos = self.cursor_camera_pos(norm_pos)[:2]
            self.initial_view = copy.copy(self.mvp.view)
            self.window.set_cursor_pos_callback(self.translate)
            self.stop_view_change = self.stop_translate

    def stop_translate(self):
        del self.stop_view_change
        self.window.set_cursor_pos_callback()
        del self.initial_view
        del self.initial_pos
        del self.initial_norm_z

    def translate(self, pos, mods):
        xscale = config.view.controls.translate.xscale
        yscale = config.view.controls.translate.yscale

        x0, y0 = self.initial_pos
        norm_x1, norm_y1 = self.cursor_normalized_window_pos(pos)
        norm_pos = np.array((norm_x1, norm_y1, self.initial_norm_z, 1))
        x1, y1 = self.cursor_camera_pos(norm_pos)[:2]

        translation = xscale * (x0 - x1) * self.initial_view.right \
                    + yscale * (y0 - y1) * self.initial_view.up
        log.debug(f'TRANSLATE {translation}')

        self.mvp.view = copy.copy(self.initial_view)
        self.mvp.view.eye += translation


class InteractiveScene(Scene):
    def __enter__(self):
        self.window.set_key_callback(self.key)
        self.window.set_scroll_callback(self.scroll)
        self.window.set_mouse_button_callback(self.mouse_button)

    def __exit__(self, *args):
        self.window.set_mouse_button_callback()
        self.window.set_scroll_callback()
        self.window.set_key_callback()

    def key(self, key, action, mods):
        log.debug(f'KEY {key} {action} {mods}')
        if action in {Action.Press, Action.Repeat}:
            dforward = config.view.controls.translate.forward_step
            dright = config.view.controls.translate.right_step
            translate = {
                Key.Up: lambda: dforward * self.mvp.view.forward,
                Key.Down: lambda: -dforward * self.mvp.view.forward,
                Key.Left: lambda: -dright * self.mvp.view.right,
                Key.Right: lambda: dright * self.mvp.view.right,
            }
            if key in translate:
                displacement = translate[key]()
                self.mvp.view.translate(displacement)
        if hasattr(self, 'on_cursor_pos'):
            self.on_cursor_pos(self.window.cursor_window_pos(), mods)

    def mouse_button(self, button, action, mods):
        log.debug(f'MOUSE BUTTON {button} {action} {mods}')
        if action == Action.Release:
            if hasattr(self, 'stop_view_change'):
                self.stop_view_change()
        elif action == Action.Press:
            if button == Button.MouseLeft:
                if mods & Mod.Control:
                    self.start_trackball()
                else:
                    self.start_arcball()
            elif button == Button.MouseRight:
                if mods & Mod.Control:
                    self.start_translate()
                else:
                    self.start_rotate()
            elif button == Button.MouseMiddle:
                pass

    def scroll(self, offset, mods):
        log.debug(f'SCROLL {offset.x} {offset.y} {mods}')
        if mods & Mod.Control:
            fovmin = config.view.controls.fov.min
            fovmax = config.view.controls.fov.max
            dfov = config.view.controls.fov.step
            fov = self.mvp.projection.fov
            newfov = min(max(fov + dfov * offset.y, fovmin), fovmax)
            log.debug(f'FOV: {newfov * 180 / np.pi:.1f}')
            self.mvp.projection.fov = newfov
