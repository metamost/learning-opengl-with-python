import glm
import numpy as np
import transforms


w, h = 640, 360

eye = np.ones(3) * 20
target = np.ones(3) * 0
up = np.array((0, 1, 0), dtype=float)

model_pos = np.array((2, 4, 4, 1), dtype=float)

print('model pos:', model_pos)

mvp = transforms.MVP()
mvp.view.eye = eye
mvp.view.look_at(target, up)

mvp.projection.fov = 45 * np.pi / 180
mvp.projection.aspect = w / h
mvp.projection.near = 1
mvp.projection.far = 100

view = mvp.view.matrix
proj = mvp.projection.matrix

screen_pos = model_pos @ view @ proj
print('screen (mvp):', screen_pos)


glm_model_pos = glm.vec4(*model_pos)
glm_eye = glm.vec3(*eye)
glm_target = glm.vec3(*target)
glm_up = glm.vec3(*up)
glm_view = glm.lookAt(glm_eye, glm_target, glm_up)
glm_proj = glm.perspective(mvp.projection.fov,
                           mvp.projection.aspect,
                           mvp.projection.near,
                           mvp.projection.far)

glm_screen_pos = glm_proj * glm_view * glm_model_pos
print('screen (glm):', glm_screen_pos)
#glm_screen_pos_norm = glm_screen_pos / glm_screen_pos[3]
#print('screen (glm):', glm_screen_pos_norm)
