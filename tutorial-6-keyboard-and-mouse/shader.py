import contextlib
import copy
import logging

from OpenGL import GL as gl
import numpy as np

from transform import ModelTransform


log = logging.getLogger(__name__)


class Model:
    def bind_vertex_buffer(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, self.vertex_data, gl.GL_STATIC_DRAW)

    @property
    def vertex_buffer(self):
        if not hasattr(self, '_vertex_buffer'):
            self._vertex_buffer = gl.glGenBuffers(1)
            self.bind_vertex_buffer()
        return self._vertex_buffer

    def bind_color_buffer(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.color_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, self.color_data, gl.GL_STREAM_DRAW)

    @property
    def color_buffer(self):
        if not hasattr(self, '_color_buffer'):
            self._color_buffer = gl.glGenBuffers(1)
            self.bind_color_buffer()
        return self._color_buffer

    @property
    def buffers(self):
        return (self.vertex_buffer, self.color_buffer)

    def bind_buffers(self):
        for i, buf in enumerate(self.buffers):
            gl.glEnableVertexAttribArray(i)  # use currently bound VAO
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buf)
            gl.glVertexAttribPointer(
                i,              # attribute number
                3,              # components per vertex attribute
                gl.GL_FLOAT,    # type
                False,          # to be normalized?
                0,              # stride
                None            # array buffer offset
            )

    def disable_vertex_attributes(self):
        for i, buf in enumerate(self.buffers):
            gl.glDisableVertexAttribArray(i)

    def delete_buffers(self):
        gl.glDeleteBuffers(len(self.buffers), self.buffers)


class ColorfulModel(Model):
    @property
    def color_data(self):
        if not hasattr(self, '_color_data'):
            self._color_data = np.random.uniform(0, 1, self.vertex_data.shape).astype(np.float32)
        return self._color_data

    def randomly_adjust_colors(self):
        adjustment = np.random.normal(0, .01, self.color_data.shape)
        self._color_data += adjustment
        self.bind_color_buffer()


class ColorfulCube(ColorfulModel):
    def __init__(self):
        self.vertex_data = 0.7 * np.array((
            (-1,-1,-1), (-1,-1, 1), (-1, 1, 1),
            ( 1, 1,-1), (-1,-1,-1), (-1, 1,-1),
            ( 1,-1, 1), (-1,-1,-1), ( 1,-1,-1),
            ( 1, 1,-1), ( 1,-1,-1), (-1,-1,-1),
            (-1,-1,-1), (-1, 1, 1), (-1, 1,-1),
            ( 1,-1, 1), (-1,-1, 1), (-1,-1,-1),
            (-1, 1, 1), (-1,-1, 1), ( 1,-1, 1),
            ( 1, 1, 1), ( 1,-1,-1), ( 1, 1,-1),
            ( 1,-1,-1), ( 1, 1, 1), ( 1,-1, 1),
            ( 1, 1, 1), ( 1, 1,-1), (-1, 1,-1),
            ( 1, 1, 1), (-1, 1,-1), (-1, 1, 1),
            ( 1, 1, 1), (-1, 1, 1), ( 1,-1, 1),
        ), dtype=np.float32)

    def draw(self):
        self.bind_buffers()
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, len(self.vertex_data))
        self.disable_vertex_attributes()


class ColorfulTriangle(ColorfulModel):
    def __init__(self):
        self.vertex_data = np.array((
            (-1, -1, 0), (1, -1, 0), (0,  1, 0),
        ), dtype=np.float32)

    def draw(self):
        self.bind_buffers()
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, len(self.vertex_data))
        self.disable_vertex_attributes()


class AxesModel(Model):
    def __init__(self):
        self.vertex_data = np.array((
            (0, 0, 0),
            (1, 0, 0),
            (0, 0, 0),
            (0, 1, 0),
            (0, 0, 0),
            (0, 0, 1),
        ), dtype=np.float32)
        self.vertex_data *= 20

        self.color_data = np.array((
            (0.7, 0.7, 0.0),
            (0.7, 0.7, 0.0),
            (0.7, 0.7, 0.0),
            (0.7, 0.7, 0.0),
            (0.7, 0.7, 0.0),
            (0.7, 0.7, 0.0),
        ), dtype=np.float32)

    def draw(self):
        self.bind_buffers()
        nlines = 3
        nvertices_per_line = 2
        gl.glDrawArrays(gl.GL_LINES, 0, nlines * nvertices_per_line)
        self.disable_vertex_attributes()


class ShaderProgram:
    def __init__(self, mvp):
        self.mvp = mvp
        self.shaders = {}  # { shader_type: shader_source, ... }
        self.models = []  # [ [ transform, [ model0, model1, ... ] ], ... ]
        self.program_id = None
        self.shader_ids = []
        self.vertex_array_id = None

    def __enter__(self):
        self.create_program()
        self.define_shaders()
        self.compile_shaders()
        self.attach_shaders()
        self.link_program()
        self.build_models()
        return self

    def __exit__(self, *args):
        for _, models in self.models:
            for model in models:
                model.delete_buffers()
        if self.vertex_array_id:
            gl.glDeleteVertexArrays(1, [self.vertex_array_id])
        for shader_id in self.shader_ids:
            gl.glDetachShader(self.program_id, shader_id)
            gl.glDeleteShader(shader_id)
        if self.program_id:
            gl.glDeleteProgram(self.program_id)

    @property
    def vertex_source(self):
        return self.shaders.get(gl.GL_VERTEX_SHADER, None)

    @vertex_source.setter
    def vertex_source(self, src):
        self.shaders[gl.GL_VERTEX_SHADER] = src

    @property
    def fragment_source(self):
        return self.shaders.get(gl.GL_FRAGMENT_SHADER, None)

    @fragment_source.setter
    def fragment_source(self, src):
        self.shaders[gl.GL_FRAGMENT_SHADER] = src

    def create_program(self):
        self.program_id = gl.glCreateProgram()

    def compile_shaders(self):
        for shader_type, shader_src in self.shaders.items():
            if shader_src:
                shader_id = gl.glCreateShader(shader_type)
                gl.glShaderSource(shader_id, shader_src)
                gl.glCompileShader(shader_id)
                if not gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS):
                    if gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH):
                        log.error(gl.glGetShaderInfoLog(shader_id))
                    else:
                        log.error('failed to compile shader (no log info)')
                self.shader_ids.append(shader_id)

    def attach_shaders(self):
        for shader_id in self.shader_ids:
            gl.glAttachShader(self.program_id, shader_id)

    def link_program(self):
        gl.glLinkProgram(self.program_id)
        if not gl.glGetProgramiv(self.program_id, gl.GL_LINK_STATUS):
            if gl.glGetProgramiv(self.program_id, gl.GL_INFO_LOG_LENGTH):
                log.error(gl.glGetProgramInfoLog(self.program_id))
            else:
                log.error('failed ot link program (no log info)')

    @contextlib.contextmanager
    def draw_context(self):
        gl.glUseProgram(self.program_id)
        gl.glBindVertexArray(self.vertex_array_id)
        yield
        gl.glBindVertexArray(0)

    def draw(self):
        with self.draw_context():
            for transform, models in self.models:
                self.mvp.model = copy.copy(transform)
                mvp_id = self.mvp.id(self.program_id)
                gl.glUniformMatrix4fv(mvp_id, 1, False, self.mvp.matrix())
                for model in models:
                    model.draw()


class ColorfulShapes(ShaderProgram):
    def build_models(self):
        self.vertex_array_id = gl.glGenVertexArrays(1)
        log.debug(f'colorful shapes vao: {self.vertex_array_id}')
        for pos in np.random.uniform(0, 20, (100, 3)):
            tform = ModelTransform()
            tform.position = pos
            models = [ColorfulCube()]
            self.models.append([tform, models])
        for pos in np.random.uniform(0, 20, (100, 3)):
            tform = ModelTransform()
            tform.position = pos
            models = [ColorfulTriangle()]
            self.models.append([tform, models])

    def define_shaders(self):
        self.vertex_source = '''\
            #version 330 core
            layout(location = 0) in vec3 vertexPosition_modelspace;
            layout(location = 1) in vec3 vertexColor;
            uniform mat4 MVP;
            out vec3 fragmentColor;
            void main()
            {
                gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
                fragmentColor = vertexColor;
            }
        '''
        self.fragment_source = '''\
            #version 330 core
            in vec3 fragmentColor;
            out vec3 color;
            void main(){
              color = fragmentColor;
            }
        '''

    def draw(self):
        with self.draw_context():
            for transform, models in self.models:
                transform.position += np.random.normal(0, 0.01, 3)
                transform.rotation += np.quaternion(*np.random.normal(0, 0.01, 4))
                self.mvp.model = copy.copy(transform)
                mvp_id = self.mvp.id(self.program_id)
                gl.glUniformMatrix4fv(mvp_id, 1, False, self.mvp.matrix())
                for model in models:
                    model.randomly_adjust_colors()
                    model.draw()


class Axes(ShaderProgram):
    def define_shaders(self):
        self.vertex_source = '''\
            #version 330 core
            layout(location = 0) in vec3 vertexPosition_modelspace;
            layout(location = 1) in vec3 vertexColor;
            uniform mat4 MVP;
            out vec3 fragmentColor;
            void main()
            {
                gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
                fragmentColor = vertexColor;
            }
        '''
        self.fragment_source = '''\
            #version 330 core
            in vec3 fragmentColor;
            out vec3 color;
            void main(){
              color = fragmentColor;
            }
        '''

    def build_models(self):
        self.vertex_array_id = gl.glGenVertexArrays(1)
        log.debug(f'axes vao: {self.vertex_array_id}')
        self.models = [ [ ModelTransform(), [ AxesModel() ] ] ]


class TargetModel:
    def __init__(self):
        #self.stride = 0
        #self.vertex_data = 10 * np.ones(3, dtype=np.float32)
        #self.color_data = np.array((0.8, 0.8, 0.), dtype=np.float32)
        from sklearn.datasets import load_iris
        self.vertex_data = load_iris()['data'].astype(np.float32)
        self.stride = len(self.vertex_data.T)
        print(self.vertex_data.shape, self.stride)
        #print(vertex_data)
        for i in range(self.stride):
            dmin, dmax = self.vertex_data[:,i].min(), self.vertex_data[:,i].max()
            print(dmin, dmax)
            self.vertex_data[...,i] = 20 * (self.vertex_data[:,i] - dmin) / (dmax - dmin)
        #print(np.array(list(vertex_data.flat)))
        #vertex_data = rand.normal(0, 0.1, 4 * 10000).astype(np.float32)
        self.vertex_buffer = gl.glGenBuffers(1)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        array_type = (gl.GLfloat * self.vertex_data.size)
        gl.glBufferData(gl.GL_ARRAY_BUFFER,
                        self.vertex_data.nbytes,
                        array_type(*self.vertex_data.flat),
                        gl.GL_STATIC_DRAW)

    def bind_buffers(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        gl.glVertexAttribPointer(
           0,                  # attribute 0.
           4,                  # components per vertex attribute
           gl.GL_FLOAT,        # type
           False,              # to be normalized?
           self.stride,
           None                 # array buffer offset
        )
        gl.glEnableVertexAttribArray(0)  # use currently bound VAO

    def delete_buffers(self):
        pass

    def draw(self):
        gl.glEnable(gl.GL_VERTEX_PROGRAM_POINT_SIZE)
        self.bind_buffers()
        gl.glDrawArrays(gl.GL_POINTS, 0, len(self.vertex_data))
        self.disable_vertex_attributes()
        gl.glDisable(gl.GL_VERTEX_PROGRAM_POINT_SIZE)

    def set_position(self, pos):
        log.debug(f'target position: {pos}')
        if pos is not None:
            self.vertex_data[:] = pos
            self.bind_vertex_buffer()

    def disable_vertex_attributes(self):
        pass


class Target(ShaderProgram):
    def define_shaders(self):
        self.vertex_source = '''\
            #version 330 core
            layout(location = 0) in vec3 vPos;
            uniform mat4 MVP;
            out vec2 screenPos;
            out float radius;
            void main(){
              gl_PointSize = 30.0;
              gl_Position = MVP * vec4(vPos, 1);  // clip space
              vec2 screenSize = vec2(1024, 768);
              screenPos = (0.5 * gl_Position.xy / gl_Position.w + 0.5) * screenSize;
              radius = 0.5 * gl_PointSize;
            }
        '''
        self.fragment_source = '''\
            #version 330 core
            in vec2 screenPos;
            in float radius;
            out vec4 color;
            float linstep(float edge0, float edge1, float x) {
                return 1 - ((x - edge0) / (edge1 - edge0));
            }
            float smoothstep_laplace(float edge0, float edge1, float x, float width) {
                return linstep(edge0, edge1, x) * exp(- abs((x - edge0) / (width * edge1)));
            }
            void main(){
              float dist = distance(gl_FragCoord.xy, screenPos);
              if (dist > radius) discard;
              vec3 base_color = vec3(1, 0, 0);
              float alpha = mix(0, 1, smoothstep_laplace(0, radius, dist, 0.7));
              color = vec4(base_color, alpha);
            }
        '''

    def build_models(self):
        self.vertex_array_id = gl.glGenVertexArrays(1)
        log.debug(f'target vao: {self.vertex_array_id}')
        self.model = TargetModel()
        self.models = [ [ ModelTransform(), [ self.model ] ] ]

    def draw(self, pos=None):
        with self.draw_context():
            for transform, models in self.models:
                self.mvp.model = copy.copy(transform)
                mvp_id = self.mvp.id(self.program_id)
                gl.glUniformMatrix4fv(mvp_id, 1, False, self.mvp.matrix())
                for model in models:
                    model.draw()
