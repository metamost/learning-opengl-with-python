import numpy as np
from scipy import linalg


def normalized(v):
    norm = linalg.norm(v)
    if norm > 0:
        return v / norm
    else:
        return v


def perspective(fov, aspect, near, far):
    t = np.tan(fov / 2) * near
    b = - t
    l, r = b * aspect, t * aspect
    n, f = near, far
    return np.array((
        ((2 * n) / (r - l),                 0,                      0,  0),
        (                0, (2 * n) / (t - b),                      0,  0),
        ((r + l) / (r - l), (t + b) / (t - b),     -(f + n) / (f - n), -1),
        (                0,                 0, -(2 * f * n) / (f - n),  0)),
        dtype=np.float32)


def ortho(left, right, bottom, top, near, far):
    l, r, b, t, n, f = left, right, bottom, top, near, far
    return np.array((
        (2 / (r - l),           0,             0, - (r + l) / (r - l)),
        (          0, 2 / (t - b),             0, - (t + b) / (t - b)),
        (          0,           0, - 2 / (f - n), - (f + n) / (f - n)),
        (          0,           0,             0,                   1)),
        dtype=np.float32)


def look_at(eye, target, up):
    eye = np.asarray(eye)
    target = np.asarray(target)
    up = np.asarray(up)
    zax = normalized(eye - target)
    xax = normalized(np.cross(up, zax))
    yax = np.cross(zax, xax)
    view = np.zeros(shape=(4,4))
    view[:3,:3] = (xax, yax, zax)
    view[:3,3] = - np.array((xax.dot(eye), yax.dot(eye), zax.dot(eye)))
    view[3,3] = 1
    return view.T
