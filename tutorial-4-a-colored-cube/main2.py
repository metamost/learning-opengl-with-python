import ctypes, ctypes.util, logging, sys, textwrap
import numpy as np
from OpenGL import GL as gl
import glfw
import glmath

log = logging.getLogger(__name__)


def load_shaders():
    shaders = {
        gl.GL_VERTEX_SHADER: textwrap.dedent('''\
            #version 330 core
            layout(location = 0) in vec3 vertexPosition_modelspace;
            layout(location = 1) in vec3 vertexColor;
            uniform mat4 MVP;
            out vec3 fragmentColor;
            void main()
            {
                gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
                fragmentColor = vertexColor;
            }
            '''),
        gl.GL_FRAGMENT_SHADER: textwrap.dedent('''\
            #version 330 core
            in vec3 fragmentColor;
            out vec3 color;
            void main(){
              color = fragmentColor;
            }
            ''')
        }

    program_id = gl.glCreateProgram()

    shader_ids = []
    for shader_type, shader_src in shaders.items():
        shader_id = gl.glCreateShader(shader_type)
        gl.glShaderSource(shader_id, shader_src)
        gl.glCompileShader(shader_id)

        result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
        info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetShaderInfoLog(shader_id)
            log.error(logmsg)

        gl.glAttachShader(program_id, shader_id)
        shader_ids.append(shader_id)

    gl.glLinkProgram(program_id)

    result = gl.glGetProgramiv(program_id, gl.GL_LINK_STATUS)
    info_log_len = gl.glGetProgramiv(program_id, gl.GL_INFO_LOG_LENGTH)
    if info_log_len:
        logmsg = gl.glGetProgramInfoLog(program_id)
        log.error(logmsg)

    for shader_id in shader_ids:
        gl.glDetachShader(program_id, shader_id)
        gl.glDeleteShader(shader_id)

    return program_id


def create_mvp(program_id, name, width, height, translate=(0,0,0)):
    fov = 45 * np.pi / 180
    aspect = width / height
    projection = glmath.perspective(fov, aspect, 0.1, 100)

    eye = np.array((4,3,-3), dtype=np.float32)
    target = np.array((0,0,0), dtype=np.float32)
    up = np.array((0,1,0), dtype=np.float32)
    translate = np.asarray(translate, dtype=np.float32)
    eye -= translate
    target -= translate

    view = glmath.look_at(eye, target, up)

    model = np.identity(4)
    mvp = model @ view @ projection
    matrix_id = gl.glGetUniformLocation(program_id, name)
    return matrix_id, mvp


def create_window(width, height):
    if not glfw.init():
        log.error('Failed to initialize GLFW')
        sys.exit(1)

    glfw.window_hint(glfw.SAMPLES, 4)  # 4x antialiasing
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)  # To make MacOS happy
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    window = glfw.create_window(width, height, 'Tutorial 03', None, None)
    if not window:
        log.error('Failed to open GLFW window.')
        glfw.terminate()
        sys.exit(2)

    glfw.make_context_current(window)
    glfw.set_input_mode(window, glfw.STICKY_KEYS, True)
    gl.glClearColor(0, 0, 0.4, 0)
    return window


def initialize_glew():
    glew = ctypes.cdll.LoadLibrary(ctypes.util.find_library('GLEW'))
    glew.experimental = ctypes.c_bool.in_dll(glew, 'glewExperimental')
    glew.init = glew.glewInit
    glew.init.restype = int

    glew.experimental = True
    if glew.init():
        log.error('Failed to initialize GLEW')
        sys.exit(3)


def create_cube():
    vertex_data = np.array((
        -1,-1,-1,
        -1,-1, 1,
        -1, 1, 1,
        1, 1,-1,
        -1,-1,-1,
        -1, 1,-1,
        1,-1, 1,
        -1,-1,-1,
        1,-1,-1,
        1, 1,-1,
        1,-1,-1,
        -1,-1,-1,
        -1,-1,-1,
        -1, 1, 1,
        -1, 1,-1,
        1,-1, 1,
        -1,-1, 1,
        -1,-1,-1,
        -1, 1, 1,
        -1,-1, 1,
        1,-1, 1,
        1, 1, 1,
        1,-1,-1,
        1, 1,-1,
        1,-1,-1,
        1, 1, 1,
        1,-1, 1,
        1, 1, 1,
        1, 1,-1,
        -1, 1,-1,
        1, 1, 1,
        -1, 1,-1,
        -1, 1, 1,
        1, 1, 1,
        -1, 1, 1,
        1,-1, 1),
        dtype=np.float32)

    n = len(vertex_data)

    #np.random.seed(1)
    color_data = np.random.uniform(0,1,n).astype(np.float32)

    vertex_buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)
    gl.glBufferData(gl.GL_ARRAY_BUFFER, vertex_data, gl.GL_STATIC_DRAW)

    color_buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, color_buffer)
    gl.glBufferData(gl.GL_ARRAY_BUFFER, color_data, gl.GL_STATIC_DRAW)

    return (vertex_buffer, n), (color_buffer, n)


def create_triangle():
    vertex_data = np.array([
            -1, -1, 0,
             1, -1, 0,
             0,  1, 0],
        dtype=np.float32)

    n = len(vertex_data)

    #np.random.seed(1)
    color_data = np.random.uniform(0,1,n).astype(np.float32)

    vertex_buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)
    gl.glBufferData(gl.GL_ARRAY_BUFFER, vertex_data, gl.GL_STATIC_DRAW)

    color_buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, color_buffer)
    gl.glBufferData(gl.GL_ARRAY_BUFFER, color_data, gl.GL_STATIC_DRAW)

    return (vertex_buffer, n), (color_buffer, n)


def create_buffers():
    vertex_array_id = gl.glGenVertexArrays(1)
    gl.glBindVertexArray(vertex_array_id)
    array_buffers = (
        create_cube(),
        create_triangle(),
    )
    return [vertex_array_id], array_buffers


def main_loop(window, program_id, array_buffers, mvps):
    def maintain_window(window):
        result = glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS
        result = result and not glfw.window_should_close(window)
        return result

    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glDepthFunc(gl.GL_LESS)

    while maintain_window(window):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        gl.glUseProgram(program_id)

        for abufs, mvpinfo in zip(array_buffers, mvps):
            for i, (buf, sz) in enumerate(abufs):

                gl.glEnableVertexAttribArray(i)
                gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buf)
                gl.glVertexAttribPointer(
                   i,
                   3,                  # size
                   gl.GL_FLOAT,        # type
                   False,              # normalized?
                   0,                  # stride
                   None                # array buffer offset
                )

            matrix_id, mvp = mvpinfo
            gl.glUniformMatrix4fv(matrix_id, 1, False, mvp)

            gl.glDrawArrays(gl.GL_TRIANGLES, 0, sz)

        gl.glDisableVertexAttribArray(0)

        glfw.swap_buffers(window)
        glfw.poll_events()


def cleanup(program_id, buffers, vertex_arrays):
    gl.glDeleteBuffers(1, buffers)
    gl.glDeleteVertexArrays(1, vertex_arrays)
    gl.glDeleteProgram(program_id)

    glfw.terminate()


def main():
    logging.basicConfig()

    width, height = 1024, 768
    window = create_window(width, height)
    initialize_glew()
    program_id = load_shaders()
    vertex_array_ids, array_buffers = create_buffers()
    mvps = (
        create_mvp(program_id, 'MVP', width, height),
        create_mvp(program_id, 'MVP', width, height, (2.5,1,0)),
    )
    main_loop(window, program_id, array_buffers, mvps)
    cleanup(program_id, array_buffers, vertex_array_ids)


if __name__ == '__main__':
    main()
