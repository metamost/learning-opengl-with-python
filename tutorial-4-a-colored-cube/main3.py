import ctypes, ctypes.util, logging, sys, textwrap
import numpy as np
from OpenGL import GL as gl
import glfw

import glmath

log = logging.getLogger(__name__)


class ShaderProgram:
    def __init__(self):
        self.vertex_src = None
        self.fragment_src = None
        self.shader_ids = []

    @property
    def id(self):
        if not hasattr(self, '_id'):
            self._id = gl.glCreateProgram()
            self.load()
        return self._id

    def load(self):
        shaders = {
            gl.GL_VERTEX_SHADER: textwrap.dedent(self.vertex_src),
            gl.GL_FRAGMENT_SHADER: textwrap.dedent(self.fragment_src),
        }

        self.shader_ids = []
        for shader_type, shader_src in shaders.items():
            shader_id = gl.glCreateShader(shader_type)
            gl.glShaderSource(shader_id, shader_src)
            gl.glCompileShader(shader_id)

            result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
            info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
            if info_log_len:
                logmsg = gl.glGetShaderInfoLog(shader_id)
                log.error(logmsg)

            gl.glAttachShader(self._id, shader_id)
            self.shader_ids.append(shader_id)

        gl.glLinkProgram(self._id)

        result = gl.glGetProgramiv(self._id, gl.GL_LINK_STATUS)
        info_log_len = gl.glGetProgramiv(self._id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetProgramInfoLog(self._id)
            log.error(logmsg)

        for shader_id in self.shader_ids:
            gl.glDetachShader(self._id, shader_id)
            gl.glDeleteShader(shader_id)


class MVP:
    def __init__(self, width, height):
        self.name = 'MVP'
        self.aspect = width / height
        self.translate = np.array((0,0,0), dtype=np.float32)

    @property
    def projection(self):
        fov = 45 * np.pi / 180
        m = glmath.perspective(fov, self.aspect, 0.1, 100)
        print('projection')
        print(m)
        return m

    @property
    def view(self):
        eye = np.array((4,3,-3), dtype=np.float32)
        target = np.array((0,0,0), dtype=np.float32)
        up = np.array((0,1,0), dtype=np.float32)
        self.translate = np.asarray(self.translate, dtype=np.float32)
        eye -= self.translate
        target -= self.translate
        m = glmath.look_at(eye, target, up)
        print('view')
        print(m)
        return m

    @property
    def model(self):
        return np.identity(4)

    @property
    def matrix(self):
        m = self.model @ self.view @ self.projection
        print('mvp')
        print(m)
        return m

    def matrix_id(self, shader_program):
        return gl.glGetUniformLocation(shader_program.id, self.name)


def create_window(width, height):
    if not glfw.init():
        log.error('Failed to initialize GLFW')
        sys.exit(1)

    glfw.window_hint(glfw.SAMPLES, 4)  # 4x antialiasing
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)  # To make MacOS happy
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    window = glfw.create_window(width, height, 'Tutorial', None, None)
    if not window:
        log.error('Failed to open GLFW window.')
        glfw.terminate()
        sys.exit(2)

    glfw.make_context_current(window)
    glfw.set_input_mode(window, glfw.STICKY_KEYS, True)
    gl.glClearColor(0, 0, 0.4, 0)
    return window


def initialize_glew():
    glew = ctypes.cdll.LoadLibrary(ctypes.util.find_library('GLEW'))
    glew.experimental = ctypes.c_bool.in_dll(glew, 'glewExperimental')
    glew.init = glew.glewInit
    glew.init.restype = int

    glew.experimental = True
    if glew.init():
        log.error('Failed to initialize GLEW')
        sys.exit(3)


class Model:
    def __len__(self):
        return len(self.vertex_data)

    @property
    def color_data(self):
        if not hasattr(self, '_color_data'):
            self._color_data = np.random.uniform(0,1,len(self)).astype(np.float32)
        return self._color_data

    @property
    def vertex_buffer(self):
        if not hasattr(self, '_vertex_buffer'):
            self._vertex_buffer = gl.glGenBuffers(1)
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self._vertex_buffer)
            gl.glBufferData(gl.GL_ARRAY_BUFFER, self.vertex_data, gl.GL_STATIC_DRAW)
        return self._vertex_buffer

    def generate_colors(self):
        data = self.color_data
        data += np.random.normal(0,0.01,len(self)).astype(np.float32)
        data = np.clip(data, 0, 1)
        self.bind_color_buffer()

    def bind_color_buffer(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.color_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, self.color_data, gl.GL_STREAM_DRAW)

    @property
    def color_buffer(self):
        if not hasattr(self, '_color_buffer'):
            self._color_buffer = gl.glGenBuffers(1)
            self.bind_color_buffer()
        return self._color_buffer

    @property
    def buffers(self):
        yield self.vertex_buffer
        yield self.color_buffer


class Cube(Model):
    def __init__(self):
        self.vertex_data = np.array((
            -1,-1,-1,  -1,-1, 1,  -1, 1, 1,
             1, 1,-1,  -1,-1,-1,  -1, 1,-1,
             1,-1, 1,  -1,-1,-1,   1,-1,-1,
             1, 1,-1,   1,-1,-1,  -1,-1,-1,
            -1,-1,-1,  -1, 1, 1,  -1, 1,-1,
             1,-1, 1,  -1,-1, 1,  -1,-1,-1,
            -1, 1, 1,  -1,-1, 1,   1,-1, 1,
             1, 1, 1,   1,-1,-1,   1, 1,-1,
             1,-1,-1,   1, 1, 1,   1,-1, 1,
             1, 1, 1,   1, 1,-1,  -1, 1,-1,
             1, 1, 1,  -1, 1,-1,  -1, 1, 1,
             1, 1, 1,  -1, 1, 1,   1,-1, 1,
            ), dtype=np.float32)


class Triangle(Model):
    def __init__(self):
        self.vertex_data = np.array((
            -1, -1, 0,  1, -1, 0,  0,  1, 0,
            ), dtype=np.float32)


def main_loop(window, shader_program, models):
    def maintain_window(window):
        result = glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS
        result = result and not glfw.window_should_close(window)
        return result

    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glDepthFunc(gl.GL_LESS)

    while maintain_window(window):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        gl.glUseProgram(shader_program.id)

        for mvp, model in models:
            model.generate_colors()
            for i, buf in enumerate(model.buffers):
                gl.glEnableVertexAttribArray(i)
                gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buf)
                gl.glVertexAttribPointer(
                   i,
                   3,                  # size
                   gl.GL_FLOAT,        # type
                   False,              # normalized?
                   0,                  # stride
                   None                # array buffer offset
                )

            gl.glUniformMatrix4fv(mvp.matrix_id(shader_program), 1, False, mvp.matrix)
            gl.glDrawArrays(gl.GL_TRIANGLES, 0, len(model))

        gl.glDisableVertexAttribArray(0)

        glfw.swap_buffers(window)
        glfw.poll_events()


def cleanup(vertex_arrays, shader_program, models):
    for mvp, model in models:
        gl.glDeleteBuffers(1, list(model.buffers))
    gl.glDeleteProgram(shader_program.id)
    gl.glDeleteVertexArrays(1, vertex_arrays)
    glfw.terminate()


def main():
    logging.basicConfig()

    width, height = 1024, 768
    window = create_window(width, height)
    initialize_glew()

    shader_program = ShaderProgram()
    shader_program.vertex_src = '''\
        #version 330 core
        layout(location = 0) in vec3 vertexPosition_modelspace;
        layout(location = 1) in vec3 vertexColor;
        uniform mat4 MVP;
        out vec3 fragmentColor;
        void main()
        {
            gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
            fragmentColor = vertexColor;
        }
    '''
    shader_program.fragment_src = '''\
        #version 330 core
        in vec3 fragmentColor;
        out vec3 color;
        void main(){
          color = fragmentColor;
        }
    '''

    # not sure if this is needed
    vertex_array_id = gl.glGenVertexArrays(1)
    gl.glBindVertexArray(vertex_array_id)

    models = (
        (MVP(width, height), Cube()),
        (MVP(width, height), Triangle()),
    )
    models[1][0].translate = (2.5, 1, 0)

    main_loop(window, shader_program, models)
    cleanup([vertex_array_id], shader_program, models)


if __name__ == '__main__':
    main()
