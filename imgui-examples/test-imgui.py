import contextlib, ctypes, logging, sys, time
import OpenGL.GL as gl
import glfw

import imgui
import imgui.integrations.glfw

log = logging.getLogger(__name__)


@contextlib.contextmanager
def create_gui():
    imgui.create_context()
    style = imgui.get_style()
    style.window_border_size = 0

    with create_main_window() as window:
        font_size_in_pixels = 32

        win_w, win_h = glfw.get_window_size(window)
        fb_w, fb_h = glfw.get_framebuffer_size(window)
        font_scaling_factor = max(fb_w / win_w, fb_h / win_h)

        io = imgui.get_io()
        io.fonts.clear()
        io.fonts.add_font_from_file_ttf(
            "/usr/share/fonts/dejavu/DejaVuSansMono.ttf",
            font_size_in_pixels * font_scaling_factor
        )
        io.font_global_scale /= font_scaling_factor

        renderer = imgui.integrations.glfw.GlfwRenderer(window)
        try:
            yield window, renderer
        finally:
            renderer.shutdown()


@contextlib.contextmanager
def create_main_window():
    if not glfw.init():
        log.error('failed to initialize GLFW')
        sys.exit(1)
    try:
        log.debug('requiring modern OpenGL without any legacy features')
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

        log.debug('opening window')
        window = glfw.create_window(1024, 768, 'Tutorial 01', None, None)
        if not window:
            log.error('failed to open GLFW window.')
            sys.exit(2)
        glfw.make_context_current(window)

        log.debug('listen for key presses')
        glfw.set_input_mode(window, glfw.STICKY_KEYS, True)

        log.debug('set background to dark blue')
        gl.glClearColor(0, 0, 0.4, 0)

        yield window

    finally:
        log.debug('terminating window context')
        glfw.terminate()


@contextlib.contextmanager
def create_vertex_array_object():
    log.debug('creating and binding the vertex array (VAO)')
    vertex_array_id = gl.glGenVertexArrays(1)
    try:
        gl.glBindVertexArray(vertex_array_id)
        yield
    finally:
        log.debug('cleaning up vertex array')
        gl.glDeleteVertexArrays(1, [vertex_array_id])


@contextlib.contextmanager
def create_vertex_buffer():
    with create_vertex_array_object():

        # A triangle
        vertex_data = [-1, -1, 0,
                        1, -1, 0,
                        0,  1, 0]

        attr_id = 0  # No particular reason for 0,
                     # but must match the layout location in the shader.

        log.debug('creating and binding the vertex buffer (VBO)')
        vertex_buffer = gl.glGenBuffers(1)
        try:
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vertex_buffer)

            array_type = (gl.GLfloat * len(vertex_data))
            gl.glBufferData(gl.GL_ARRAY_BUFFER,
                            len(vertex_data) * ctypes.sizeof(ctypes.c_float),
                            array_type(*vertex_data),
                            gl.GL_STATIC_DRAW)

            log.debug('setting the vertex attributes')
            gl.glVertexAttribPointer(
               attr_id,            # attribute 0.
               3,                  # components per vertex attribute
               gl.GL_FLOAT,        # type
               False,              # to be normalized?
               0,                  # stride
               None                # array buffer offset
            )
            gl.glEnableVertexAttribArray(attr_id)  # use currently bound VAO
            yield
        finally:
            log.debug('cleaning up buffer')
            gl.glDisableVertexAttribArray(attr_id)
            gl.glDeleteBuffers(1, [vertex_buffer])


@contextlib.contextmanager
def load_shaders():
    shaders = {
        gl.GL_VERTEX_SHADER: '''\
            #version 330 core
            layout(location = 0) in vec3 vertexPosition_modelspace;
            void main(){
              gl_Position.xyz = vertexPosition_modelspace;
              gl_Position.w = 1.0;
            }
            ''',
        gl.GL_FRAGMENT_SHADER: '''\
            #version 330 core
            out vec3 color;
            void main(){
              color = vec3(1,0,0);
            }
            '''
        }
    log.debug('creating the shader program')
    program_id = gl.glCreateProgram()
    try:
        shader_ids = []
        for shader_type, shader_src in shaders.items():
            shader_id = gl.glCreateShader(shader_type)
            gl.glShaderSource(shader_id, shader_src)

            log.debug(f'compiling the {shader_type} shader')
            gl.glCompileShader(shader_id)

            # check if the compilation was successful by getting the
            # status and associated log
            result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
            info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
            if info_log_len:
                logmsg = gl.glGetShaderInfoLog(shader_id)
                log.error(logmsg)
                sys.exit(10)

            gl.glAttachShader(program_id, shader_id)
            shader_ids.append(shader_id)

        log.debug('linking shader program')
        gl.glLinkProgram(program_id)

        result = gl.glGetProgramiv(program_id, gl.GL_LINK_STATUS)
        info_log_len = gl.glGetProgramiv(program_id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetProgramInfoLog(program_id)
            log.error(logmsg)
            sys.exit(11)

        for shader_id in shader_ids:
            gl.glDetachShader(program_id, shader_id)
            gl.glDeleteShader(shader_id)

        log.debug('installing shader program into rendering state')
        gl.glUseProgram(program_id)
        yield
    finally:
        log.debug('cleaning up shader program')
        gl.glUseProgram(0)
        gl.glDeleteProgram(program_id)


def build_gui(render_time):
    imgui.new_frame()
    imgui.set_next_window_position(10, 10)
    imgui.set_next_window_bg_alpha(0.0)
    imgui.begin('', False, imgui.WINDOW_NO_TITLE_BAR         | \
                           imgui.WINDOW_NO_RESIZE            | \
                           imgui.WINDOW_NO_MOVE              | \
                           imgui.WINDOW_NO_SCROLLBAR         | \
                           imgui.WINDOW_NO_SCROLL_WITH_MOUSE | \
                           imgui.WINDOW_NO_COLLAPSE          | \
                           imgui.WINDOW_NO_SAVED_SETTINGS    | \
                           imgui.WINDOW_NO_INPUTS)
    imgui.text_colored("{:.3f} ms".format(render_time), 0.4, 0.8, 0.9)
    imgui.end()


def main_loop(window, renderer):
    start = time.perf_counter()
    frame_count = 0
    render_time = 0

    while (
        glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS and
        not glfw.window_should_close(window)
    ):
        frame_count += 1
        if time.perf_counter() - start > 1.0:
            render_time = 1000 / frame_count
            start = time.perf_counter()
            frame_count = 0

        glfw.poll_events()
        renderer.process_inputs()
        build_gui(render_time)

        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        # Draw the triangle
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, 3)  # Starting from vertex 0
                                                # 3 vertices total -> 1 triangle

        imgui.render()
        renderer.render(imgui.get_draw_data())
        glfw.swap_buffers(window)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    with create_gui() as (window, renderer):
        with create_vertex_buffer():
            with load_shaders():
                main_loop(window, renderer)
