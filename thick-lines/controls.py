import copy, logging

import numpy as np
import quaternion
from scipy import linalg

from OpenGL import GL as gl
import glfw

from transforms import normalized, rotation_between_vectors


log = logging.getLogger(__name__)
#logging.basicConfig(level=logging.DEBUG)


class config:
    class view:
        class controls:
            class arcball:
                xscale = 2.1 * np.pi
                yscale = 2.1 * np.pi
                axis = np.array((0, 1, 0))
            class trackball:
                xscale = 1.1 * np.pi
                yscale = 1.1 * np.pi
            class rotate:
                scale = 1.0
            class fov:
                min = 25 * np.pi / 180
                max = 85 * np.pi / 180
                step = 1 * np.pi / 180
            class translate:
                forward_step = 1
                right_step = 1
                xscale = 1.0
                yscale = 1.0


class Scene:
    def __init__(self, window, mvp, target):
        self.window = window
        self.mvp = mvp
        self.target = target

    @property
    def window_size(self):
        if not hasattr(self, '_window_size'):
            self._window_size = np.asarray(glfw.get_window_size(self.window))
        return self._window_size

    @property
    def framebuffer_size(self):
        if not hasattr(self, '_framebuffer_size'):
            fb_size = glfw.get_framebuffer_size(self.window)
            self._framebuffer_size = np.asarray(fb_size)
        return self._framebuffer_size

    def cursor_window_pos(self):
        '''
            cursor position in window/screen display coordinates
            display pixels (x, y) with origin at left top
                units: pixels
                origin: left top
                x: left to right
                y: top to bottom
        '''
        pos = np.asarray(glfw.get_cursor_pos(self.window))
        log.debug(f'  CURSOR WIN POS: ({int(pos[0])}, {int(pos[1])})')
        return pos

    def cursor_normalized_window_pos(self, window_pos=None):
        '''
            cursor position (center of pixel) (x, y) where
            x, y are normalized to the range [-1, 1]
                origin: center
                x: left to right
                y: top to bottom
        '''
        if window_pos is None:
            window_pos = self.cursor_window_pos()
        else:
            window_pos = np.asarray(window_pos)
        win_size = self.window_size
        pos = 2 * (window_pos + np.array((0.5, 0.5))) / win_size - 1
        pos[1] *= -1
        log.debug(f'  CURSOR NORM WIN POS: ({pos[0]:.3f}, {pos[1]:.3f})')
        return pos

    def cursor_framebuffer_pos(self, window_pos=None):
        '''
            cursor position in framebuffer coordinates
            framebuffer pixels (x, y) with origin at left top
                units: pixels
                origin: left top
                x: left to right
                y: top to bottom
        '''
        if window_pos is None:
            window_pos = self.cursor_window_pos()
        else:
            window_pos = np.asarray(window_pos)
        scaling = self.framebuffer_size / self.window_size
        pos = window_pos * scaling
        log.debug(f'  CURSOR FBUF POS: ({int(pos[0])}, {int(pos[1])})')
        return pos

    def cursor_normalized_pos(self, framebuffer_pos=None):
        '''
            normalized device (screen) coordinates (x, y, z, w=1)
            where x, y, z are in the range [-1, 1]
                origin: center
                x: left to right
                y: top to bottom
                z: out of the screen to the viewer
        '''
        if framebuffer_pos is None:
            framebuffer_pos = self.cursor_framebuffer_pos()
        else:
            framebuffer_pos = np.asarray(framebuffer_pos)

        # cursor position of pixel center in framebuffer coordinates
        # units: (pixel width, pixel height)
        # origin: left top
        pos = framebuffer_pos + np.array((0.5, 0.5))

        # get depth at cursor position
        # range: [0, 1]
        y = self.framebuffer_size[1] - pos[1] - 1
        z = gl.glReadPixels(pos[0], y, 1, 1,
                            gl.GL_DEPTH_COMPONENT, gl.GL_FLOAT)

        # normalize depth to range [-1, 1]
        z = 2 * z[0][0] - 1

        # cursor position in normalized device (screen) coordinates
        # where x, y, z are in the range [-1, 1]
        pos = 2 * pos / self.framebuffer_size - 1
        pos = np.array((pos[0], - pos[1], z, 1))
        log.debug(f'  CURSOR NORM POS: ({pos[0]:.3f}, {pos[1]:.3f}, {pos[2]:.3f})')
        return pos

    def cursor_camera_pos(self, normalized_pos=None, inv_proj=None):
        '''
            position in camera (view) coordinates (x, y, z, w=1)
            cursor position in world space where the origin is the camera
            position
                z-axis: forward
                y-axis: up
                x-axis: right
        '''

        if normalized_pos is None:
            normalized_pos = self.cursor_normalized_pos()
        else:
            normalized_pos = np.asarray(normalized_pos)
        if inv_proj is None:
            inv_proj = linalg.inv(self.mvp.projection.matrix)
        pos = normalized_pos @ inv_proj
        pos /= pos[3]
        log.debug(f'  CURSOR CAMERA POS: ({pos[0]:.3f}, {pos[1]:.3f}, {pos[2]:.3f})')
        return pos

    def cursor_world_pos(self, camera_pos=None, inv_view=None):
        '''
            cursor position in world coordinates (x, y, z, w=1)
        '''
        if camera_pos is None:
            camera_pos = self.cursor_camera_pos()
        else:
            camera_pos = np.asarray(camera_pos)
        if inv_view is None:
            inv_view = linalg.inv(self.mvp.view.matrix)
        pos = camera_pos @ inv_view
        pos /= pos[3]
        log.debug(f'  CURSOR WORLD POS: ({pos[0]:.3f}, {pos[1]:.3f}, {pos[2]:.3f})')
        return pos

    def eye_to_cursor(self, world_pos=None):
        '''
            eye to cursor vector in world space
        '''
        if world_pos is None:
            world_pos = self.cursor_world_pos()
        else:
            world_pos = np.asarray(world_pos)
        return world_pos[:3] - self.mvp.view.eye

    ################ ARCBALL ###################################################
    def start_arcball(self):
        if not hasattr(self, 'stop_view_change'):
            self.initial_cursor_pos = self.cursor_normalized_window_pos()
            self.initial_view = copy.copy(self.mvp.view)
            self.on_cursor_pos = self.arcball
            glfw.set_cursor_pos_callback(self.window, self.on_cursor_pos)
            self.stop_view_change = self.stop_arcball

    def stop_arcball(self):
        del self.stop_view_change
        glfw.set_cursor_pos_callback(self.window, lambda *a, **kw: None)
        del self.on_cursor_pos
        del self.initial_view
        del self.initial_cursor_pos

    def arcball(self, window, x, y):
        ctrl, shft, alt = self.modifiers(window)
        xscale = config.view.controls.arcball.xscale
        yscale = config.view.controls.arcball.yscale

        x0, y0 = self.initial_cursor_pos
        v0 = self.initial_view
        x1, y1 = self.cursor_normalized_window_pos((x, y))

        if ctrl:
            log.debug(f'ARCBALL {x0} {y0} {x1} {y1}')
            upvec = xscale * (x0 - x1) * v0.up
            rightvec = yscale * (y1 - y0) * v0.right
            rotation = quaternion.from_rotation_vector(upvec + rightvec)
        elif alt:
            log.debug(f'CAMERA-UP-AXIS ARCBALL {x0} {y0} {x1} {y1}')
            upvec = xscale * (x0 - x1) * v0.up
            uprot = quaternion.from_rotation_vector(upvec)
            newright = quaternion.as_rotation_matrix(uprot) @ v0.right
            newrightvec = yscale * (y1 - y0) * newright
            rightrot = quaternion.from_rotation_vector(newrightvec)
            rotation = rightrot * uprot
        else:
            log.debug(f'WORLD-AXIS ARCBALL {x0} {y0} {x1} {y1}')
            # maybe better to use the closest axis to the "up" direction
            axis = config.view.controls.arcball.axis
            axangle = xscale * (x0 - x1) * axis
            axrot = quaternion.from_rotation_vector(axangle)
            rangle = yscale * (y1 - y0) * np.cross(v0.forward, axis)
            rrot = quaternion.from_rotation_vector(rangle)
            rotation = axrot * rrot

        self.mvp.view = copy.copy(v0)
        self.mvp.view.rotate_about(rotation, self.target)

    ################ TRACKBALL #################################################
    def start_trackball(self):
        if not hasattr(self, 'stop_view_change'):
            self.initial_cursor_pos = self.cursor_normalized_window_pos()
            glfw.set_cursor_pos_callback(self.window, self.trackball)
            self.stop_view_change = self.stop_trackball

    def stop_trackball(self):
        del self.stop_view_change
        glfw.set_cursor_pos_callback(self.window, lambda *a, **kw: None)
        del self.initial_cursor_pos

    def trackball(self, window, x, y):
        xscale = config.view.controls.trackball.xscale
        yscale = config.view.controls.trackball.yscale

        x0, y0 = self.initial_cursor_pos
        x1, y1 = self.cursor_normalized_window_pos((x, y))
        log.debug(f'TRACKBALL {x0} {y0} {x1} {y1}')

        upvec = xscale * (x0 - x1) * self.mvp.view.up
        rightvec = yscale * (y1 - y0) * self.mvp.view.right
        rotation = quaternion.from_rotation_vector(upvec + rightvec)

        self.mvp.view.rotate_about(rotation, self.target)
        self.initial_cursor_pos = np.array((x1, y1))

    ############## ROTATE ######################################################
    def start_rotate(self):
        if not hasattr(self, 'stop_view_change'):
            norm_pos = self.cursor_normalized_pos()
            camera_pos = self.cursor_camera_pos(norm_pos)
            pos = self.cursor_world_pos(camera_pos)[:3]
            self.initial_norm_z = norm_pos[2]
            self.initial_eye_to_cursor = pos - self.mvp.view.eye
            self.initial_view = copy.copy(self.mvp.view)
            glfw.set_cursor_pos_callback(self.window, self.rotate)
            self.stop_view_change = self.stop_rotate

    def stop_rotate(self):
        del self.stop_view_change
        glfw.set_cursor_pos_callback(self.window, lambda *a, **kw: None)
        del self.initial_view
        del self.initial_eye_to_cursor
        del self.initial_norm_z

    def rotate(self, window, x, y):
        scale = config.view.controls.rotate.scale

        v0 = self.initial_eye_to_cursor
        x1, y1 = self.cursor_normalized_window_pos((x, y))
        norm_pos = np.array((x1, y1, self.initial_norm_z, 1))
        camera_pos = self.cursor_camera_pos(norm_pos)
        pos = self.cursor_world_pos(camera_pos, self.initial_view.inv_matrix)[:3]
        v1 = pos - self.initial_view.eye

        log.debug(f'ROTATE {v0} -> {v1}')
        rotation = rotation_between_vectors(v1, v0)
        rotation.w /= scale

        self.mvp.view = copy.copy(self.initial_view)
        self.mvp.view.rotate(rotation)

    ############## TRANSLATE ###################################################
    def start_translate(self):
        if not hasattr(self, 'stop_view_change'):
            norm_pos = self.cursor_normalized_pos()
            self.initial_norm_z = norm_pos[2]
            self.initial_pos = self.cursor_camera_pos(norm_pos)[:2]
            self.initial_view = copy.copy(self.mvp.view)
            glfw.set_cursor_pos_callback(self.window, self.translate)
            self.stop_view_change = self.stop_translate

    def stop_translate(self):
        del self.stop_view_change
        glfw.set_cursor_pos_callback(self.window, lambda *a, **kw: None)
        del self.initial_view
        del self.initial_pos
        del self.initial_norm_z

    def translate(self, window, x, y):
        xscale = config.view.controls.translate.xscale
        yscale = config.view.controls.translate.yscale

        x0, y0 = self.initial_pos
        norm_x1, norm_y1 = self.cursor_normalized_window_pos((x, y))
        norm_pos = np.array((norm_x1, norm_y1, self.initial_norm_z, 1))
        x1, y1 = self.cursor_camera_pos(norm_pos)[:2]

        translation = xscale * (x0 - x1) * self.initial_view.right \
                    + yscale * (y0 - y1) * self.initial_view.up
        log.debug(f'TRANSLATE {translation}')

        self.mvp.view = copy.copy(self.initial_view)
        self.mvp.view.eye = self.mvp.view.eye + translation
        self.target += translation

class Controls(Scene):
    def __enter__(self):
        glfw.set_key_callback(self.window, self.key)
        glfw.set_scroll_callback(self.window, self.scroll)
        glfw.set_mouse_button_callback(self.window, self.mouse_button)

    def __exit__(self, *args):
        glfw.set_mouse_button_callback(self.window, None)
        glfw.set_scroll_callback(self.window, None)
        glfw.set_key_callback(self.window, None)

    def modifiers(self, window):
        lctrl = glfw.get_key(window, glfw.KEY_LEFT_CONTROL) == glfw.PRESS
        rctrl = glfw.get_key(window, glfw.KEY_RIGHT_CONTROL) == glfw.PRESS
        lshft = glfw.get_key(window, glfw.KEY_LEFT_SHIFT) == glfw.PRESS
        rshft = glfw.get_key(window, glfw.KEY_RIGHT_SHIFT) == glfw.PRESS
        lalt = glfw.get_key(window, glfw.KEY_LEFT_ALT) == glfw.PRESS
        ralt = glfw.get_key(window, glfw.KEY_RIGHT_ALT) == glfw.PRESS
        ctrl = lctrl or rctrl
        shft = lshft or rshft
        alt = lalt or ralt
        return ctrl, shft, alt

    def key(self, window, key, scancode, action, mods):
        log.debug(f'KEY {key} {scancode} {action} {mods}')
        if action in (glfw.PRESS, glfw.REPEAT):
            dforward = config.view.controls.translate.forward_step
            dright = config.view.controls.translate.right_step
            translate = {
                glfw.KEY_UP: lambda: dforward * self.mvp.view.forward,
                glfw.KEY_DOWN: lambda: -dforward * self.mvp.view.forward,
                glfw.KEY_LEFT: lambda: -dright * self.mvp.view.right,
                glfw.KEY_RIGHT: lambda: dright * self.mvp.view.right,
            }
            if key in translate:
                displacement = translate[key]()
                self.mvp.view.eye += displacement
                self.target += displacement
        if hasattr(self, 'on_cursor_pos'):
            self.on_cursor_pos(window, *glfw.get_cursor_pos(window))

    def mouse_button(self, window, button, action, mods):
        log.debug(f'MOUSE BUTTON {button} {action} {mods}')
        if action == glfw.RELEASE:
            if hasattr(self, 'stop_view_change'):
                self.stop_view_change()
        elif action == glfw.PRESS:
            if button == glfw.MOUSE_BUTTON_LEFT:
                if mods & glfw.MOD_CONTROL:
                    self.start_trackball()
                else:
                    self.start_arcball()
            if button == glfw.MOUSE_BUTTON_RIGHT:
                if mods & glfw.MOD_CONTROL:
                    self.start_translate()
                else:
                    self.start_rotate()
            if button == glfw.MOUSE_BUTTON_MIDDLE:
                pass

    def scroll(self, window, xoffset, yoffset):
        log.debug(f'SCROLL {xoffset} {yoffset}')
        ctrl, shft, alt = self.modifiers(window)
        if ctrl:
            fovmin = config.view.controls.fov.min
            fovmax = config.view.controls.fov.max
            dfov = config.view.controls.fov.step
            fov = self.mvp.projection.fov
            newfov = min(max(fov + dfov * yoffset, fovmin), fovmax)
            log.debug(f'FOV: {newfov * 180 / np.pi:.1f}')
            self.mvp.projection.fov = newfov
