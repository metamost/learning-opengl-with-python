import numpy as np
import quaternion
from scipy import linalg

from OpenGL import GL as gl

def normalized(v):
    norm = linalg.norm(v)
    if norm > 0:
        return v / norm
    else:
        return v


def rotation_between_vectors(u, v):
    # normalize vectors or return a non-rotation
    u_norm = linalg.norm(u)
    if u_norm > 0:
        u = u / u_norm
    else:
        return np.quaternion(1, 0, 0, 0)

    v_norm = linalg.norm(v)
    if v_norm > 0:
        v = v / v_norm
    else:
        return np.quaternion(1, 0, 0, 0)

    cos_theta = 1. + u.dot(v)
    if cos_theta < 1e-6:
        """
        If u and v are exactly opposite, rotate 180 degrees
        around an arbitrary orthogonal axis. Axis normalization
        can happen later, when we normalize the quaternion.
        """
        cos_theta = 0.
        if np.abs(u[0]) > np.abs(u[2]):
            w = np.array((-u[1], u[0], 0))
        else:
            w = np.array((0., -u[2], u[1]))
    else:
        """
        Otherwise, build quaternion the standard way.
        """
        w = np.cross(u, v)

    return np.quaternion(cos_theta, w[0], w[1], w[2]).normalized()


class ModelTransform:
    def __init__(self):
        self._scale = np.full(3, 1)
        self._position = np.zeros(3)
        self._rotation = np.quaternion(1, 0, 0, 0)
        self._M = None
        self._T = None
        self._R = None

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = np.asarray(value)
        self._M, self._T = None, None

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = np.asarray(value)
        self._M, self._T = None, None

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        self._rotation = np.quaternion(value)
        self._M, self._R = None, None

    @property
    def matrix(self):
        if self._M is None:
            if self._T is None:
                sx, sy, sz = self._scale
                x, y, z = self._position
                self._T = np.array(((sx,  0,  0, 0),
                                    ( 0, sy,  0, 0),
                                    ( 0,  0, sz, 0),
                                    ( x,  y,  z, 1)))
            if self._R is None:
                self._R = np.identity(4)
                self._R[:3,:3] = quaternion.as_rotation_matrix(self._rotation)
            self._M = self._R @ self._T
        return self._M


class ViewTransform:
    def __init__(self):
        self._eye = np.ones(3)
        self.look_at(np.zeros(3), np.array((0,1,0)))

    @property
    def eye(self):
        if self._eye is None:
            self._eye = linalg.inv(self.matrix)[3,:3]
        return self._eye

    @eye.setter
    def eye(self, position):
        self._eye = np.asarray(position)
        _ = self.rotation  # ensure rotation is saved
        self._matrix = None
        self._inv_matrix = None

    @property
    def rotation(self):
        if self._rotation is None:
            self._rotation = quaternion.from_rotation_matrix(self.matrix[:3,:3])
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        self._rotation = np.quaternion(value)
        _ = self.eye  # ensure eye position is saved
        self._matrix = None
        self._inv_matrix = None

    @property
    def matrix(self):
        if self._matrix is None:
            R = self.rotation_matrix
            self._matrix = np.identity(4)
            self._matrix[:3,:3] = R
            self._matrix[3,:3] = [- v.dot(self.eye) for v in R.T]
        return self._matrix

    @matrix.setter
    def matrix(self, value):
        self._matrix = value
        self._inv_matrix = None
        self._eye = None
        self._rotation = None

    @property
    def inv_matrix(self):
        if self._inv_matrix is None:
            self._inv_matrix = linalg.inv(self.matrix)
        return self._inv_matrix

    @property
    def rotation_matrix(self):
        return quaternion.as_rotation_matrix(self.rotation)

    @rotation_matrix.setter
    def rotation_matrix(self, value):
        self.rotation = quaternion.from_rotation_matrix(value)

    def look_at(self, target, up):
        zax = normalized(self.eye - np.asarray(target))
        xax = normalized(np.cross(np.asarray(up), zax))
        yax = np.cross(zax, xax)
        self.rotation_matrix = np.stack((xax, yax, zax), axis=1)

    def rotate(self, rotation):
        self.rotation = rotation * self.rotation

    def rotate_about(self, rotation, origin=np.zeros(3)):
        T0, R, T1 = np.identity(4), np.identity(4), np.identity(4)
        T0[3,:3] = - origin
        R[:3,:3] = quaternion.as_rotation_matrix(rotation)
        T1[3,:3] = origin
        M = T0 @ R @ T1
        self.matrix = M @ self.matrix

    def translate(self, vector):
        target = self.eye + self.forward
        self.eye = self.eye + vector
        self.look_at(target + vector, self.up)

    @property
    def right(self):
        return self.matrix[:3,0]

    @property
    def up(self):
        return self.matrix[:3,1]

    @property
    def forward(self):
        return - self.matrix[:3,2]


class PerspectiveTransform:
    def __init__(self):
        self._aspect = 4 / 3
        self._fov = 45 * np.pi / 180
        self._near = 0.1
        self._far = 1.
        self._M = None
        self._inv_matrix = None

    @property
    def aspect(self):
        return self._aspect

    @aspect.setter
    def aspect(self, value):
        self._aspect = value
        self._M = None
        self._inv_matrix = None

    @property
    def fov(self):
        return self._fov

    @fov.setter
    def fov(self, value):
        self._fov = value
        self._M = None
        self._inv_matrix = None

    @property
    def near(self):
        return self._near

    @near.setter
    def near(self, value):
        self._near = value
        self._M = None
        self._inv_matrix = None

    @property
    def far(self):
        return self._far

    @far.setter
    def far(self, value):
        self._far = value
        self._M = None
        self._inv_matrix = None

    @property
    def matrix(self):
        if self._M is None:
            t = np.tan(self._fov / 2) * self._near
            b = - t
            assert abs(t - b) > 0
            l, r = b * self._aspect, t * self._aspect
            assert abs(r - l) > 0
            n, f = self._near, self._far
            assert abs(n - f) > 0
            self._M = np.array((
                (  2 * n / (r - l),                 0,                     0,  0),
                (                0,   2 * n / (t - b),                     0,  0),
                ((r + l) / (r - l), (t + b) / (t - b),     (f + n) / (n - f), -1),
                (                0,                 0, 2 * (f * n) / (n - f),  0)))
        return self._M

    @property
    def inv_matrix(self):
        if self._inv_matrix is None:
            self._inv_matrix = linalg.inv(self.matrix)
        return self._inv_matrix


class OrthographicTransform:
    def __init__(self):
        self.left = 0.
        self.right = 1.
        self.bottom = 0.
        self.top = 1.
        self.near = 0.1
        self.far = 1.

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, value):
        self._left = value
        self._M = None

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, value):
        self._right = value
        self._M = None

    @property
    def bottom(self):
        return self._bottom

    @bottom.setter
    def bottom(self, value):
        self._bottom = value
        self._M = None

    @property
    def top(self):
        return self._top

    @top.setter
    def top(self, value):
        self._top = value
        self._M = None

    @property
    def near(self):
        return self._near

    @near.setter
    def near(self, value):
        self._near = value
        self._M = None

    @property
    def far(self):
        return self._far

    @far.setter
    def far(self, value):
        self._far = value
        self._M = None

    @property
    def matrix(self):
        if self._M is None:
            l, r = self.left, self.right
            b, t = self.bottom, self.top
            n, f = self.near, self.far
            assert abs(r - l) > 0
            assert abs(t - b) > 0
            assert abs(n - f) > 0
            self._M = np.array((
                (2 / (r - l),           0,           0, (r + l) / (l - r)),
                (          0, 2 / (t - b),           0, (t + b) / (b - t)),
                (          0,           0, 2 / (n - f), (f + n) / (n - f)),
                (          0,           0,           0,                 1)))
        return self._M


class MVPTransform:
    def __init__(self):
        self.model = ModelTransform()
        self.view = ViewTransform()
        self.projection = PerspectiveTransform()

    def matrix(self):
        m = self.model.matrix @ self.view.matrix @ self.projection.matrix
        return m.astype(np.float32)


class MVP(MVPTransform):
    def __init__(self):
        self.name = 'MVP'
        super().__init__()

    def id(self, shader_program):
        return gl.glGetUniformLocation(shader_program.id, self.name)
