import ctypes, ctypes.util, copy, logging, sys, textwrap
import numpy as np
import quaternion
from OpenGL import GL as gl
import glfw

import controls, transforms, shaders

log = logging.getLogger(__name__)


class Model:
    def __len__(self):
        return len(self.vertex_data)

    @property
    def color_data(self):
        if not hasattr(self, '_color_data'):
            self._color_data = np.random.uniform(0,1,len(self)).astype(np.float32)
        return self._color_data

    @property
    def vertex_buffer(self):
        if not hasattr(self, '_vertex_buffer'):
            self._vertex_buffer = gl.glGenBuffers(1)
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self._vertex_buffer)
            gl.glBufferData(gl.GL_ARRAY_BUFFER, self.vertex_data, gl.GL_STATIC_DRAW)
        return self._vertex_buffer

    def generate_colors(self):
        data = self.color_data
        data += np.random.normal(0,0.01,len(self)).astype(np.float32)
        data = np.clip(data, 0, 1)
        self.bind_color_buffer()

    def bind_color_buffer(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.color_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, self.color_data, gl.GL_STREAM_DRAW)

    @property
    def color_buffer(self):
        if not hasattr(self, '_color_buffer'):
            self._color_buffer = gl.glGenBuffers(1)
            self.bind_color_buffer()
        return self._color_buffer

    @property
    def buffers(self):
        yield self.vertex_buffer
        yield self.color_buffer


class Cube(Model):
    def __init__(self):
        self.vertex_data = np.array((
            -1,-1,-1,  -1,-1, 1,  -1, 1, 1,
             1, 1,-1,  -1,-1,-1,  -1, 1,-1,
             1,-1, 1,  -1,-1,-1,   1,-1,-1,
             1, 1,-1,   1,-1,-1,  -1,-1,-1,
            -1,-1,-1,  -1, 1, 1,  -1, 1,-1,
             1,-1, 1,  -1,-1, 1,  -1,-1,-1,
            -1, 1, 1,  -1,-1, 1,   1,-1, 1,
             1, 1, 1,   1,-1,-1,   1, 1,-1,
             1,-1,-1,   1, 1, 1,   1,-1, 1,
             1, 1, 1,   1, 1,-1,  -1, 1,-1,
             1, 1, 1,  -1, 1,-1,  -1, 1, 1,
             1, 1, 1,  -1, 1, 1,   1,-1, 1,
            ), dtype=np.float32)


class Triangle(Model):
    def __init__(self):
        self.vertex_data = np.array((
            -1, -1, 0,  1, -1, 0,  0,  1, 0,
            ), dtype=np.float32)


class Axes:
    def __enter__(self):
        self.vertex_data = np.array([
            [0, 0, 0],
            [1, 0, 0],
            [0, 0, 0],
            [0, 1, 0],
            [0, 0, 0],
            [0, 0, 1],], dtype=np.float32)
        self.vertex_data *= 20

        self.color_data = np.array([
            [0.7, 0.7, 0.0],
            [0.7, 0.7, 0.0],
            [0.7, 0.7, 0.0],
            [0.7, 0.7, 0.0],
            [0.7, 0.7, 0.0],
            [0.7, 0.7, 0.0],], dtype=np.float32)

        self.vertex_array_id = gl.glGenVertexArrays(1)
        print('axes vao:', self.vertex_array_id)
        gl.glBindVertexArray(self.vertex_array_id)
        self.vertex_buffer, self.color_buffer = gl.glGenBuffers(2)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        array_type = (gl.GLfloat * self.vertex_data.size)
        gl.glBufferData(gl.GL_ARRAY_BUFFER,
                        self.vertex_data.nbytes,
                        array_type(*self.vertex_data.flat),
                        gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(
           0,                  # attribute number
           3,                  # components per vertex attribute
           gl.GL_FLOAT,        # type
           False,              # to be normalized?
           3 * 4,
           None # array buffer offset
        )
        gl.glEnableVertexAttribArray(0)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.color_buffer)
        array_type = (gl.GLfloat * self.color_data.size)
        gl.glBufferData(gl.GL_ARRAY_BUFFER,
                        self.color_data.nbytes,
                        array_type(*self.color_data.flat),
                        gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(
           1,                  # attribute number
           3,                  # components per vertex attribute
           gl.GL_FLOAT,        # type
           False,              # to be normalized?
           3 * 4,
           None # array buffer offset
        )
        gl.glEnableVertexAttribArray(1)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)

        return self

    def __exit__(self, *args):
        gl.glDeleteBuffers(1, [self.vertex_buffer])
        gl.glDeleteVertexArrays(1, [self.vertex_array_id])

    def draw(self):
        gl.glBindVertexArray(self.vertex_array_id)
        nlines = 3
        nvertices_per_line = 2
        gl.glDrawArrays(gl.GL_LINES, 0, nlines * nvertices_per_line)


