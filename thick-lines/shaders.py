import logging, textwrap
from OpenGL import GL as gl

log = logging.getLogger(__name__)


class ShaderProgram:
    def __init__(self):
        self.vertex_src = None
        self.fragment_src = None
        self.shader_ids = []

    @property
    def id(self):
        if not hasattr(self, '_id'):
            self._id = gl.glCreateProgram()
            self.load()
        return self._id

    def load(self):
        shaders = {
            gl.GL_VERTEX_SHADER: textwrap.dedent(self.vertex_src),
            gl.GL_FRAGMENT_SHADER: textwrap.dedent(self.fragment_src),
        }

        self.shader_ids = []
        for shader_type, shader_src in shaders.items():
            shader_id = gl.glCreateShader(shader_type)
            gl.glShaderSource(shader_id, shader_src)
            gl.glCompileShader(shader_id)

            result = gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS)
            info_log_len = gl.glGetShaderiv(shader_id, gl.GL_INFO_LOG_LENGTH)
            if info_log_len:
                logmsg = gl.glGetShaderInfoLog(shader_id)
                log.error(logmsg)

            gl.glAttachShader(self._id, shader_id)
            self.shader_ids.append(shader_id)

        gl.glLinkProgram(self._id)

        result = gl.glGetProgramiv(self._id, gl.GL_LINK_STATUS)
        info_log_len = gl.glGetProgramiv(self._id, gl.GL_INFO_LOG_LENGTH)
        if info_log_len:
            logmsg = gl.glGetProgramInfoLog(self._id)
            log.error(logmsg)

        for shader_id in self.shader_ids:
            gl.glDetachShader(self._id, shader_id)
            gl.glDeleteShader(shader_id)


def place_and_color_program():
    shader_program = ShaderProgram()
    shader_program.vertex_src = '''\
        #version 330 core
        layout(location = 0) in vec3 vertexPosition_modelspace;
        layout(location = 1) in vec3 vertexColor;
        uniform mat4 MVP;
        out vec3 fragmentColor;
        void main()
        {
            gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
            fragmentColor = vertexColor;
        }
    '''
    shader_program.fragment_src = '''\
        #version 330 core
        in vec3 fragmentColor;
        out vec3 color;
        void main(){
          color = fragmentColor;
        }
    '''
    return shader_program


def thin_line_on_screen_program():
    raise NotImplementedError


def thick_line_on_screen_program():
    raise NotImplementedError
