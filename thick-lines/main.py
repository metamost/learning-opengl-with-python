import ctypes, ctypes.util, copy, logging, sys, textwrap
import numpy as np
import quaternion
from OpenGL import GL as gl
import glfw

import controls, transforms, shaders, models

log = logging.getLogger(__name__)


def create_window(width, height):
    if not glfw.init():
        log.error('Failed to initialize GLFW')
        sys.exit(1)

    glfw.window_hint(glfw.SAMPLES, 4)  # 4x antialiasing
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, True)  # To make MacOS happy
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    window = glfw.create_window(width, height, 'Tutorial', None, None)
    if not window:
        log.error('Failed to open GLFW window.')
        glfw.terminate()
        sys.exit(2)

    glfw.make_context_current(window)
    glfw.set_input_mode(window, glfw.STICKY_KEYS, False)
    gl.glClearColor(0, 0, 0.4, 0)
    return window


def initialize_glew():
    glew = ctypes.cdll.LoadLibrary(ctypes.util.find_library('GLEW'))
    glew.experimental = ctypes.c_bool.in_dll(glew, 'glewExperimental')
    glew.init = glew.glewInit
    glew.init.restype = int

    glew.experimental = True
    if glew.init():
        log.error('Failed to initialize GLEW')
        sys.exit(3)


def scene_control(window, mvp):
    mvp = transforms.MVP()
    eye = np.ones(3) * 30
    target = np.ones(3) * 10
    up = transforms.normalized(np.array((0,1,0)))

    mvp.view.eye = eye
    mvp.view.look_at(target, up)

    mvp.projection.near = 0.1
    mvp.projection.far = 100

    with controls.Controls(window, mvp, target), \
         Axes() as axes:
             yield


def main_loop(window, shader_program, models, model_vertex_array_id):
    def maintain_window(window):
        result = glfw.get_key(window, glfw.KEY_ESCAPE) != glfw.PRESS
        result = result and not glfw.window_should_close(window)
        return result

    gl.glEnable(gl.GL_MULTISAMPLE)
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glDepthFunc(gl.GL_LESS)


        while maintain_window(window):
            gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

            gl.glBindVertexArray(model_vertex_array_id)

            gl.glUseProgram(shader_program.id)

            for transform, model in models:
                transform.position += np.random.normal(0, 0.01, 3)
                transform.rotation += np.quaternion(*np.random.normal(0, 0.01, 4))
                mvp.model = copy.copy(transform)
                model.generate_colors()
                for i, buf in enumerate(model.buffers):
                    gl.glEnableVertexAttribArray(i)  # use currently bound VAO
                    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buf)
                    gl.glVertexAttribPointer(
                       i,
                       3,                  # size
                       gl.GL_FLOAT,        # type
                       False,              # normalized?
                       0,                  # stride
                       None                # array buffer offset
                    )

                gl.glUniformMatrix4fv(mvp.id(shader_program), 1, False, mvp.matrix())
                gl.glDrawArrays(gl.GL_TRIANGLES, 0, len(model))

            mvp.model.position = np.array((0,0,0))
            mvp.model.rotation = np.quaternion(1, 0, 0, 0)
            gl.glUniformMatrix4fv(mvp.id(shader_program), 1, False, mvp.matrix())
            axes.draw()
            gl.glBindVertexArray(0)

            gl.glDisableVertexAttribArray(0)

            glfw.swap_buffers(window)
            glfw.poll_events()


def cleanup(vertex_arrays, shader_program, models):
    for mvp, model in models:
        gl.glDeleteBuffers(1, list(model.buffers))
    gl.glDeleteProgram(shader_program.id)
    gl.glDeleteVertexArrays(1, vertex_arrays)
    glfw.terminate()


def main():
    logging.basicConfig()

    width, height = 1024, 768
    window = create_window(width, height)
    initialize_glew()

    vertex_array_id = gl.glGenVertexArrays(1)
    print('model vao:', vertex_array_id)
    gl.glBindVertexArray(vertex_array_id)

    models = []
    for pos in np.random.uniform(0, 20, (10, 3)):
        models.append((transforms.ModelTransform(), Cube()))
        models[-1][0].position = pos

    for pos in np.random.uniform(0, 20, (10, 3)):
        models.append((transforms.ModelTransform(), Triangle()))
        models[-1][0].position = pos

    main_loop(window, shader_program, models, vertex_array_id)
    cleanup([vertex_array_id], shader_program, models)


if __name__ == '__main__':
    main()
